package vn.hoapm.optimize;

public class AlphaA implements Alpha{
    private String type;
    public AlphaA (String  type) {
        this.type = type;
    }
    @Override
    public void execute() {
        System.out.println(type);
    }
}
