package vn.hoapm.optimize;

public class BetaB implements Beta{
    private String type;
    public BetaB (String  type) {
        this.type = type;
    }
    @Override
    public void execute() {
        System.out.println(type);
    }
}
