package vn.hoapm.optimize;

public interface Beta {
    void execute();

    default void execute(String type) {
        System.out.println(type);
    }
}
