package vn.hoapm.optimize;

public interface Alpha {
    void execute();

    default void execute(String type) {
        System.out.println(type);
    }
}
