package vn.hoapm.optimize;

public class BetaC implements Beta{
    private String type;
    public BetaC (String  type) {
        this.type = type;
    }
    @Override
    public void execute() {
        System.out.println(type);
    }
}
