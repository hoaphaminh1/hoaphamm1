package vn.hoapm.optimize;

public class Main {
    public static void main(String[] args) {
        Alpha alphaA = new AlphaA("alpha A");
        Alpha alphaB = new AlphaB("alpha B");
        Beta betaB = new BetaB("betaB");
        Beta betaC = new BetaC("betac");
        betaB.execute();
        alphaA.execute();
        betaC.execute();
        alphaB.execute();
//        Alpha objecA = (Alpha) new Object();
//        objecA.execute("default A");
//        Beta objecb = (Beta) new Object();
//        objecb.execute("default B");
    }
}
