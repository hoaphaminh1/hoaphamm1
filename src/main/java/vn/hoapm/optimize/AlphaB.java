package vn.hoapm.optimize;

public class AlphaB implements Alpha{
    private String type;
    public AlphaB (String  type) {
        this.type = type;
    }
    @Override
    public void execute() {
        System.out.println(type);
    }
}
