package vn.hoapm.optimize;

public class BetaA implements Beta{
    private String type;
    public BetaA (String  type) {
        this.type = type;
    }
    @Override
    public void execute() {
        System.out.println(type);
    }
}
