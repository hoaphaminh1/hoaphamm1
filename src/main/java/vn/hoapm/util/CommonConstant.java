/*
  @copyright by Essensys and NashTesh VN
*/

package vn.hoapm.util;


/**
 * The final class contains all needed constants on system.
 *
 *
 */
public final class CommonConstant {

	public static final String MOBILE_APP_PIN = "essen435mob123";
    public static final int HOSTMASK = 32;
    public static final int MIN_PREFIX = 8;
    public static final String GATEWAY = "Gateway";
    public static final Integer VOICEMAILPIN = 1111;
    public static final int TEN_MINUTES = 10;

    public static final int PASSWORD_MIN_LENGTH = 8;
    public static final int PASSWORD_MAX_LENGTH = 50;

    public static final int EMAIL_MAX_LENGTH = 80;

    public static final String EMPTY = "";
    public static final String SQL_SEPARATE = "','";
    public static final String COMMA = ",";
    public static final String FREQUENCY_ONE_OFF = "One Off";

    public static final String THIRD_PARTY_NAME_VOICE = "Voice";
    public static final String THIRD_PARTY_NAME_SALESFORCE = "Salesforce";
    public static final String THIRD_PARTY_NAME_BILLING = "Billing";
    public static final String THIRD_PARTY_NAME_BANDWIDTH = "Bandwidth";
    public static final String THIRD_PARTY_NAME_NETWORK = "Network";

    public static final String MODIFY_ROOM_OPERATION_ADD_OCCUPIER = "ADD_OCCUPIER";
    public static final String MODIFY_ROOM_OPERATION_REMOVE_OCCUPIER = "REMOVE_OCCUPIER";

    /** PBX vendor - BroadSoft */
    public static final String PBX_VENDOR_BROADSOFT = "BROADSOFT";
    /** PBX vendor - Mitel */
    public static final String PBX_VENDOR_MITEL = "MITEL";
    public static final Integer SYSTEM_BROADSOFT_PBX_ID = 9;

    public static final String EOG_PROFILE = "Executive Offices Group";
    public static final String EOG_HUNT_DEVICE = "Executive Offices Group_HuntDevice";
    public static final String EOG_ALIAS = "EOG";

    public static final String DEVICE_TYPE_HUNT = "_HuntDevice";

    public static final String CLIENT = "client";

    public static final String FORWARD_NUMBER = "forwardNumber";
    public static final String OPTION = "option";

    public static final String PROPERTIES_JEFF_URL = "jeff.url";
    public static final String SET_PASS_LINK = "/auth/set-password?data=";
    public static final String UPDATE_PASS_AND_TERM_LINK = "/auth/user-confirm-term?data=";
    public static final String PROPERTIES_TERM_VERSION = "term.version";
    public static final String PROPERTIES_LOCK_DURATION_MINUTES = "account.lock.duration.min";
    public static final Integer NUMBER_LOGIN_FAIL_WARNING = 3;
    public static final Integer NUMBER_LOGIN_FAIL_LOCK_ACCOUNT = 10;

    public static final Integer ROOM_NAME_LENGTH = 30;
    public static final Integer FLOOR_PORT_NAME_LENGTH = 20;

    public static final String RANGE_NAME_FOR_MOVE_CLIENT_SITE = "ClientMoveRange";
    public static final String UNDERSCORE = "_";
    public static final String ONE_SPACE = " ";
    public static final String QUOTE = "\"";
    public static final String DOT = ".";
    public static final String SLASH = "/";
    public static final String SIP_PEER_PROFILES_NAME = "VTLSIP";

    public static final String HOLDING_GROUP_SUFFIX = ".holding";
    public static final String COLONS = ":";

    public static final String ROOMID_TOKENIZER = "-";
    public static final String WIFI_PROFILE_URL= "wifi.profile.url";
    public static final String UTF_8 = "UTF-8";
    public static final String TIER1 = "TIER1";
    public static final String TIER2 = "TIER2";
    public static final String TIER3 = "TIER3";

    public static final String DEFAULT_OFFSET = "0";
    public static final String DEFAULT_LIMIT = "50";

    public static final String WIFI_SOLUTION_RUCKUS_NAME = "RUCKUS";
    public static final String CLUB_WIFI_CUSTOMER_NAME = "Lounge Wifi";
    public static final String WIFI_SOLUTION_CISCO_MERAKI_NAME = "CISCO MERAKI";

    public static final int DAILY_OVERAGE_DECIMAL_PLACE = 2;
    public static final int MONTHLY_OVERAGE_DECIMAL_PLACE = 2;
    public static final int DAILY_PRICE_OVERAGE_REPORT_DECIMAL_PLACE = 4;

    public static final String AC_GROUP_NON_MEMBER_RULE_TAG = "NON_MEMBER";
    public static final String LANGUAGE_PARAM = "language=";
    public static final String EXT = "EXT_";
    public static final String EXT_ATTENDANT_FILES = "EXT_ATTENDANT_FILES_";
    public static final String EXT_ATTENDANT_FILE = "EXT_ATTENDANT_FILE_";

    public static final String SPECIAL_FULL_NAME_MAINTENANCE_CONTACT = "Maintenance Contact";

    public static final String SPECIAL_FULL_NAME_SITE_KEY_CONTACT = "Site Key Contact";
    public static String HUNT_OFFICE = "_OFFICE_HG";

    /** Usage Billing */
    public static final String CONFERENCE_BRIDGE_ID = "Conference Bridge ID";
    public static final String CONFERENCE_BRIDGE_DDI = "Conference Bridge DDI";
    public static final String DEDICATED_PIPE_NAME = "Dedicated";

    public static final String USER_ID = "User ID";
    public static final String FROM_DATE = "From Date";
    public static final String TO_DATE = "To Date";
    public static final String TIME_ZONE = "TimeZone";


    private CommonConstant() {

    }
}
