package vn.hoapm.util;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import vn.hoapm.util.functional.Try;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.StringReader;
import java.io.StringWriter;
import java.text.Normalizer;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


/**
 * Utilities
 **/
public final class TextUtils {

    private static final String XML_DECLARATION = "<?xml version='1.0' encoding='UTF-8'?>";

    private TextUtils() {

    }

    public static String prettyPrintXML(String input) {
        return prettyPrintXML(input, false);
    }

    public static String prettyPrintXML(String input, boolean isEscapeHtml) {
        if (StringUtils.isBlank(input)) {
            return StringUtils.EMPTY;
        }

        return Try.of(() -> {
            Source xmlInput = new StreamSource(new StringReader(input));
            StreamResult xmlOutput = new StreamResult(new StringWriter());
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            transformerFactory.setAttribute("indent-number", 4);
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");

            transformer.transform(xmlInput, xmlOutput);

            String output = xmlOutput.getWriter().toString();

            return isEscapeHtml ? StringEscapeUtils.escapeHtml4(output) : output;
        }).orElse(isEscapeHtml ? StringEscapeUtils.escapeHtml4(input) : input);
    }

    public static String minimizedXmlString(String input) {
        return StringUtils.trimToEmpty(input).replaceAll(">\\s+<", "><");
    }

    public static String addXmlDeclaration(String str) {
        return XML_DECLARATION + StringUtils.trimToEmpty(str);
    }

    public static String removeXmlDeclaration(String str) {
        return StringUtils.replace(str, XML_DECLARATION, "");
    }

    public static String convertUtf8ToASCII(String str) {
        String nameUTF8 = Normalizer.normalize(str, Normalizer.Form.NFKD);
        return Normalizer
                .normalize(nameUTF8, Normalizer.Form.NFD)
                .replaceAll("[^\\p{ASCII}]", "");
    }

    public static String formatListString(List<String> values) {
        return Optional.ofNullable(values).orElse(Collections.emptyList())
                .stream()
                .collect(Collectors.joining(", "));
    }

    public static String stripAccents(String input) {
        if (StringUtils.isEmpty(input)) {
            return StringUtils.EMPTY;
        }
        return StringUtils.stripAccents(input)
                .replace("Đ", "D")
                .replaceAll("[đð]", "d")
                .replaceAll("[¡¿]", "i");
    }

    public static String stripAccentsKeepSupportedCharacters(String input) {
        return stripAccents(input)
                .replaceAll("[^a-zA-Z0-9ÆæØøŒœÞþ ,.'-]", StringUtils.EMPTY);
    }
}
