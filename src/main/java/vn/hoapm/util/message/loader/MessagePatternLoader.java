
package vn.hoapm.util.message.loader;

import java.util.Map;

/**
 * The interface for messasge pattern loader.
 * 
 *
 */
public interface MessagePatternLoader {

    /**
     * Load data from source file.
     * 
     * @return data as Map (key-value).
     */
    public Map<String, String> load();
}
