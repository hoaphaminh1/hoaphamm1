
package vn.hoapm.util.message.loader;

import java.util.HashMap;
import java.util.Map;

/**
 * The loader for loading data from MS Excel file, use <a href="http://poi.apache.org/">Apache POI</a> library.
 * 
 *
 */
public class ExcelLoader implements MessagePatternLoader {

    @Override
    public Map<String, String> load() {
        return new HashMap<String, String>(1);
    }
}
