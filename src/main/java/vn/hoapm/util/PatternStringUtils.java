package vn.hoapm.util;


import org.apache.commons.lang.StringUtils;
import vn.hoapm.util.functional.Try;

import javax.swing.text.MaskFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

public class PatternStringUtils {

    public static final String US_PHONE_NUMBERS_FORMAT = "(AAA) AAA-AAAA";
    public static final String AC_DOOR_NFC_CODE_FORMAT = "\\b[0-9a-fA-F]{14}\\b";
    public static final String AC_DOOR_QR_CODE_FORMAT = "\\b[0-9a-fA-F]{8}-([0-9a-fA-F]{4}-){3}[0-9a-fA-F]{12}\\b";
    public static final String COMMON_TIME_FORMAT = "([0-1][0-9]|[2][0-3]):([0-5][0-9])";
    public static final String AC_DOOR_DAYS_OF_WEEK_FORMAT = "[X0]{7}";
    public static final String UNLOCK_SCHEDULE_REGEX = "^R:[X0]{7},[0-9]{2}:[0-9]{2},[0-9]{2}:[0-9]{2},U$";
    public static final String FLOOR_PLAN_FILE_NAME_REGEX = "[A-Za-z0-9!\\-_'*.() ]+";
    public static final String PRINTABLE_ASCII_CHARACTER_REGEX = "^[\\u0020-\\u007E]*$";
    public static final String TIME_IN_12H_FORMAT = "^(1[0-2]|0?[1-9]):[0-5][0-9] (a|A|p|P)(m|M)$";

    private static final Map<String, MaskFormatter> FORMARTTER_CACHE = new ConcurrentHashMap<>();


    /**
     * Gets the substring after the last occurrence of a regex pattern. The separator is not returned.
     *
     * @param str     the String to get a substring from, may be null
     * @param pattern the String pattern to search for, may be null
     * @return the substring after the last occurrence of the separator,
     * <code>null</code> if null String input
     * @see StringUtils#substringAfterLast(String, String)
     * @since 2.0
     */
    public static String substringAfterLastRegex(String str, String pattern) {
        if (Objects.isNull(str)) {
            return null;
        }

        if (Objects.isNull(pattern)) {
            return "";
        }

        Matcher switchPortMatcher;
        try {
            switchPortMatcher = Pattern.compile(pattern)
                    .matcher(str);
        } catch (PatternSyntaxException e) {
            return "";
        }

        int regexIndex = str.length();
        while (switchPortMatcher.find()) {
            regexIndex = switchPortMatcher.end();
        }

        return str.substring(regexIndex);
    }

    public static String maskFormat(String str, String pattern) throws Exception {
        return FORMARTTER_CACHE
                .computeIfAbsent(pattern, p -> {
                    MaskFormatter formatter = Try.of(() -> new MaskFormatter(p)).orElse(new MaskFormatter());
                    formatter.setValueContainsLiteralCharacters(false);

                    return formatter;
                })
                .valueToString(str);
    }

    public static boolean validateStringPattern(String stringNeedValidate, String pattern) {
        if (Objects.isNull(stringNeedValidate)) {
            return false;
        }
        Pattern p = Pattern.compile(pattern);
        Matcher m = p.matcher(stringNeedValidate);
        return m.matches();
    }

    public static Map<String, String> extractACDoorDescription(String input, String siteAlias) {
        Map<String, String> result = new HashMap<>();
        String[] splitData = input.split("\\(" + siteAlias + " floor"); //SAC Door description format
        splitData[1] = splitData[1].substring(0, splitData[1].length() - 1).trim(); // Remove last parenthesis
        result.put("door", splitData[0].trim());
        result.put("floor", splitData[1]);
        return result;
    }

    public static String escapeSpecialCharactersForES(String inputString){
        final String[] metaCharacters = {"+", "-", "=", "&&", "||", ">", "<", "!", "(", ")", "{", "}", "[", "]", "^", "~", "*", "?", ":", "/" };
        if (Objects.nonNull(inputString)) {
            for (int i = 0 ; i < metaCharacters.length ; i++) {
                if(inputString.contains(metaCharacters[i])){
                    inputString = inputString.replace(metaCharacters[i],"\\\\" +metaCharacters[i]);
                }
            }
        }
        return inputString;
    }

    public static String mask(String input) {
        return Try.of(() -> org.apache.commons.lang3.StringUtils.join(
                StringUtils.substring(input, 0, 1),
                StringUtils.substring(input, 1, input.length() - 2).replaceAll(".", "*"),
                StringUtils.substring(input, input.length() - 2, input.length())
        )).orElse("**************");
    }

}
