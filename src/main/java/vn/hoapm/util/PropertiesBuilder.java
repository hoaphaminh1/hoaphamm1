
package vn.hoapm.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * The utility class for building {@link Properties}.
 * 
 *
 */
public class PropertiesBuilder {

    /** The map of parameters. */
    private Map<String, String> props;

    /**
     * Should use method {@link PropertiesBuilder#newBuilder()} to create new instance of this class.
     */
    private PropertiesBuilder() {
        props = new HashMap<>();
    }

    /**
     * Create new properties builder.
     * 
     * @return the new instance of this class.
     */
    public static PropertiesBuilder newBuilder() {
        return new PropertiesBuilder();
    }

    /**
     * Add a pair of property key & property value to the map.
     * 
     * @param key
     *            the property key.
     * @param value
     *            the property value.
     * @return the current instance of this class.
     */
    public PropertiesBuilder add(String key, String value) {
        this.props.put(key, value);
        return this;
    }

    public PropertiesBuilder addAll(Map<String, String> properties) {
        this.props.putAll(properties);
        return this;
    }

    /**
     * Build properties instance.
     * 
     * @return the properties instance.
     */
    public Properties build() {
        Properties props = new Properties();
        props.putAll(this.props);

        return props;
    }

    /**
     * Build the map which contains all properties.
     * 
     * @return the map which contains all properties.
     */
    public Map<String, String> buildMap() {
        return this.props;
    }
}
