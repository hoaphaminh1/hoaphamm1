
package vn.hoapm.util;

/**
 * Defines the methods required by objects wishing to be listable by name when part of a @ ListableSet } . This Collection allows all of the element
 * names to be produced in a comma separated string.
 *
 *
 */
public interface ListableByName {

    /**
     * Returns the name of the implementing object as a string.
     *
     * @return the name of the implementing object
     */
    String getName();

}
