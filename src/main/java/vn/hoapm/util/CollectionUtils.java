package vn.hoapm.util;



import org.apache.commons.lang3.StringUtils;
import vn.hoapm.util.functional.Maybe;

import java.util.*;

/**
 */
public class CollectionUtils {

    public static Map.Entry<String, Object> param(String key, Object value) {
        return new AbstractMap.SimpleEntry<>(StringUtils.defaultIfEmpty(key, StringUtils.EMPTY), value);
    }

    @SafeVarargs
    public static <V> Map<String, V> buildMap(Map.Entry<String, V>... entries) {
        Map<String, V> inputs = new LinkedHashMap<>();
        return buildMap(inputs, entries);
    }

    @SafeVarargs
    public static <V> Map<String, V> buildMap(Map<String, V> inputs, Map.Entry<String, V>... entries) {
        Objects.requireNonNull(inputs);
        for (Map.Entry<String, V> entry : entries) {
            if (Objects.nonNull(entry)) {
                inputs.put(StringUtils.defaultIfEmpty(entry.getKey(), org.apache.commons.lang.StringUtils.EMPTY), entry.getValue());
            }
        }

        return inputs;
    }

    public static Map.Entry<String, Object>[] params(Map<String, Object> params) {
        return (Map.Entry<String, Object>[]) params.entrySet().stream()
                .map(entry -> new AbstractMap.SimpleEntry<>(StringUtils.defaultIfEmpty(entry.getKey(), StringUtils.EMPTY), entry.getValue()))
                .toArray(Map.Entry[]::new);
    }


    /**
     * Checks if the list has an initial element.
     *
     * @return {@code Nothing()} if the list is empty, else {@code Something(list.get(0))}
     * @apiNote the result may contain {@code Something(null)} as the contract just checks for element existence.
     * An additional {@code .filter(Objects::nonNull)} should be applied if safety is required
     */
    public static <E> Maybe<E> head(List<E> list) {
        return Objects.requireNonNull(list, "list").isEmpty() ? Maybe.nothing() : Maybe.something(list.get(0));
    }
    
    
    /**
     * Returns {@code true} if the specified {@code Collection} is not {@code null} and {@link Collection#isEmpty not empty},
     * {@code false} otherwise.
     *
     * @param c the collection to check
     * @return {@code true} if the specified {@code Collection} is not {@code null} and {@link Collection#isEmpty not empty},
     *         {@code false} otherwise.
     */
    public static boolean isNotEmpty(Collection<?> c) {
        return c != null &&  !c.isEmpty();
    }
    
    /**
     * Returns {@code true} if the specified {@code Map} is not {@code null} and {@link Map#isEmpty not empty},
     * {@code false} otherwise.
     *
     * @param c the collection to check
     * @return {@code true} if the specified {@code Map} is not {@code null} and {@link Map#isEmpty not empty},
     *         {@code false} otherwise.
     */
    public static boolean isNotEmpty(Map<?, ?> map) {
        return map != null  && !map.isEmpty();
    }
    
    /**
     * Returns {@code true} if the specified {@code Collection} is {@code null} or {@link Collection#isEmpty is empty},
     * {@code false} otherwise.
     *
     * @param c the collection to check
     * @return {@code true} if the specified {@code Collection} is {@code null} or {@link Collection#isEmpty is empty},
     *         {@code false} otherwise.
     */
    public static boolean isEmpty(Collection<?> c) {
        return c == null || c.isEmpty();
    }

    public static int getSize(Collection<?> c) {
        return c != null ? c.size() : 0;
    }
}
