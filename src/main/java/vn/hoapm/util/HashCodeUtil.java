
package vn.hoapm.util;

import java.lang.reflect.Array;

/**
 * Collected methods which allow easy implementation of <code>hashCode</code>.
 *
 *
 * Shamelessly taken from Effective Java by J. Bloch
 *
 * Example use case:
 *
 * <pre>
 * 
 * public int hashCode() {
 *     int result = HashCodeUtil.SEED;
 *     // collect the contributions of various fields
 *     result = HashCodeUtil.hash(result, fPrimitive);
 *     result = HashCodeUtil.hash(result, fObject);
 *     result = HashCodeUtil.hash(result, fArray);
 *     return result;
 * }
 * </pre>
 */
public final class HashCodeUtil {

    /**
     * An initial value for a <code>hashCode</code>, to which is added contributions from fields. Using a non-zero value decreases collisons of
     * <code>hashCode</code> values.
     */
    public static final int SEED = 23;

    /**
     * Bit length for long field.
     */
    public static final int LONG = 32;

    /**
     * booleans.
     * 
     * @param aSeed
     *            the Seed Value
     * @param aBoolean
     *            the boolean to Hash
     * @return Hash Value
     */
    public static int hash(final int aSeed, final boolean aBoolean) {
        int retVal = 0;

        if (aBoolean) {
            retVal = 1;
        }
        return firstTerm(aSeed) + retVal;
    }

    /**
     * chars.
     * 
     * @param aSeed
     *            the Seed Value
     * @param aChar
     *            the Char to Hash
     * @return Hash Value
     */
    public static int hash(final int aSeed, final char aChar) {
        return firstTerm(aSeed) + ((int) aChar);
    }

    /**
     * ints.
     * 
     * @param aSeed
     *            the Seed Value
     * @param aInt
     *            the Int to Hash
     * @return Hash Value
     */
    public static int hash(final int aSeed, final int aInt) {
        /*
         * Implementation Note Note that byte and short are handled by this method, through implicit conversion.
         */
        return firstTerm(aSeed) + aInt;
    }

    /**
     * longs.
     * 
     * @param aSeed
     *            the Seed Value
     * @param aLong
     *            the Long to Hash
     * @return Hash Value
     */
    public static int hash(final int aSeed, final long aLong) {
        return firstTerm(aSeed) + ((int) (aLong ^ (aLong >>> LONG)));
    }

    /**
     * floats.
     * 
     * @param aSeed
     *            the Seed Value
     * @param aFloat
     *            the Float to Hash
     * @return Hash Value
     */
    public static int hash(final int aSeed, final float aFloat) {
        return hash(aSeed, Float.floatToIntBits(aFloat));
    }

    /**
     * doubles.
     * 
     * @param aSeed
     *            the Seed Value
     * @param aDouble
     *            the Double to Hash
     * @return Hash Value
     */
    public static int hash(final int aSeed, final double aDouble) {
        return hash(aSeed, Double.doubleToLongBits(aDouble));
    }

    /**
     * <code>aObject</code> is a possibly-null object field, and possibly an array.
     *
     * @param aSeed
     *            the Seed Value
     * @param aObject
     *            the Object to Hash
     *
     *            If <code>aObject</code> is an array, then each element may be a primitive or a possibly-null object.
     * @return Hash Value
     */
    public static int hash(final int aSeed, final Object aObject) {
        int result = aSeed;
        if (aObject == null) {
            result = hash(result, 0);
        } else if (!isArray(aObject)) {
            result = hash(result, aObject.hashCode());
        } else {
            int length = Array.getLength(aObject);
            for (int idx = 0; idx < length; ++idx) {
                Object item = Array.get(aObject, idx);
                // recursive call!
                result = hash(result, item);
            }
        }
        return result;
    }

    // / PRIVATE ///
    /**
     * A Default Prime.
     */
    private static final int FODD_PRIME_NUMBER = 37;

    /**
     * Build a first Hash Term.
     *
     * @param aSeed
     *            the Seed Value
     * @return the Term
     */
    private static int firstTerm(final int aSeed) {
        return FODD_PRIME_NUMBER * aSeed;
    }

    /**
     * Verify if the Object is an Array.
     *
     * @param aObject
     *            the Object to test
     * @return true if aObject is an Array
     */
    private static boolean isArray(final Object aObject) {
        return aObject.getClass().isArray();
    }

    /**
     * Private Constructor for Util Class.
     */
    private HashCodeUtil() {
    }
}
