package vn.hoapm.util;

import org.apache.commons.lang.StringUtils;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Provide a mechanism to validate text with specified pattern
 *
 */
public final class RegexUtils {
    private RegexUtils() {
        throw new AssertionError("Utility class");
    }

    /**
     * Validate a text represent an AC card
     *
     * @param cardText : text represent an AC card
     * @return true if card contains only [0-9] with at least 1 character and max 255 characters
     * otherwise return false
     */
    public static boolean isValidCard(String cardText) {
        return StringUtils.isNotEmpty(cardText) && Pattern.compile("^[0-9]{1,255}$").matcher(cardText).find();
    }

    /**
     * Check the given text is a valid IEEE 802 MAC-48 address
     *
     * @param macAddress string to be check
     * @return true if given string is valid mac address otherwise return false
     */
    public static boolean isValidMACAddress(String macAddress) {
        if (macAddress == null) {
            return false;
        }
        return Pattern.compile("^(?:(?:[0-9A-F]{2}:){5}|(?:[0-9A-F]{2}-){5})(?:[0-9A-F]{2})$", Pattern.CASE_INSENSITIVE).matcher(macAddress).find();
    }

    /**
     * return number of digits after decimal point of a double value
     *
     * @param value string value denote a double value
     * @return number of fractional part of a double value
     */
    public static int getLengthOfFractionalPart(String value) {
        return getFractionPart(value).length();
    }

    /**
     * return fraction part of string value represent a double
     * String must have been verified that it can be converted to Double
     *
     * @param value a double value
     * @return fractional part of the given double value
     */
    public static String getFractionPart(String value) {
        if (StringUtils.isBlank(value) || !StringUtils.contains(value, CommonConstant.DOT)) {
            return StringUtils.EMPTY;
        }
        BigDecimal bd = new BigDecimal(value);
        BigDecimal fractionPart = bd.remainder(BigDecimal.ONE).abs();  // 123.112 -> 0.112  | -12314.01231 -> 0.01231
        StringBuilder sb = new StringBuilder(fractionPart.stripTrailingZeros().toString());
        sb.delete(0, 2);
        return sb.toString();
    }

    public static boolean isValidPhoneNumber(String phoneNumber) {
        return Pattern.compile("^(?:[+*]?(?:[\\d\\s]{1,20}))?$").matcher(phoneNumber).find();
    }

    public static boolean isValidName(String name) {
        String regex = "^[a-zA-Z\\u00A1\\u00BF\\u00C4\\u00E4\\u00C0\\u00E0\\u00C1\\u00E1\\u00C2\\u00E2\\u00C3\\u00E3\\u00C5\\u00E5\\u01CD\\u01CE\\u0104\\u0105\\u0102\\u0103\\u00C6\\u00E6\\u0100\\u0101\\u00C7\\u00E7\\u0106\\u0107\\u0108\\u0109\\u010C\\u010D\\u010E\\u0111\\u0110\\u010F\\u00F0\\u00C8\\u00E8\\u00C9\\u00E9\\u00CA\\u00EA\\u00CB\\u00EB\\u011A\\u011B\\u0118\\u0119\\u0116\\u0117\\u0112\\u0113\\u011C\\u011D\\u0122\\u0123\\u011E\\u011F\\u0124\\u0125\\u00CC\\u00EC\\u00CD\\u00ED\\u00CE\\u00EE\\u00CF\\u00EF\\u0131\\u012A\\u012B\\u012E\\u012F\\u0134\\u0135\\u0136\\u0137\\u0139\\u013A\\u013B\\u013C\\u0141\\u0142\\u013D\\u013E\\u00D1\\u00F1\\u0143\\u0144\\u0147\\u0148\\u0145\\u0146\\u00D6\\u00F6\\u00D2\\u00F2\\u00D3\\u00F3\\u00D4\\u00F4\\u00D5\\u00F5\\u0150\\u0151\\u00D8\\u00F8\\u0152\\u0153\\u0154\\u0155\\u0158\\u0159\\u1E9E\\u00DF\\u015A\\u015B\\u015C\\u015D\\u015E\\u015F\\u0160\\u0161\\u0218\\u0219\\u0164\\u0165\\u0162\\u0163\\u00DE\\u00FE\\u021A\\u021B\\u00DC\\u00FC\\u00D9\\u00F9\\u00DA\\u00FA\\u00DB\\u00FB\\u0170\\u0171\\u0168\\u0169\\u0172\\u0173\\u016E\\u016F\\u016A\\u016B\\u0174\\u0175\\u00DD\\u00FD\\u0178\\u00FF\\u0176\\u0177\\u0179\\u017A\\u017D\\u017E\\u017B\\u017C ,.'-]{0,30}$";
        return Pattern.compile(regex).matcher(name).find();
    }

    public static String makeFileNameSafely(String fileName) {
        return TextUtils.stripAccents(fileName)
                .replaceAll("\\s\\s*", "_")
                .replaceAll("[^a-z0-9A-Z/!_.*'()-]", StringUtils.EMPTY);
    }

    public static String removeNonDigit(String input) {
        if (input == null) {
            return null;
        }
        return input.replaceAll("[^0-9]", StringUtils.EMPTY);
    }

    public static List<String> splitCsv(String input) {
        if (StringUtils.isBlank(input)) {
            return Collections.emptyList();
        }
        return Arrays.stream(input.split("[,;\r\n]+")).collect(Collectors.toList());
    }
}
