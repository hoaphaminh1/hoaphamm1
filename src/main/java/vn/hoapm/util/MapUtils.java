package vn.hoapm.util;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.stream.Collectors;

public class MapUtils {

    public static final Logger LOGGER = LoggerFactory.getLogger(MapUtils.class);


    public static boolean populate(Map<String, Object> source, String basePath, Map<String, Object> params) {
        return populate(
                source,
                params.entrySet().stream()
                        .map(entry ->
                                CollectionUtils.param(
                                        StringUtils.isBlank(basePath) ? entry.getKey() : StringUtils.join(".", basePath, entry.getKey()),
                                        entry.getValue()
                                )
                        )
                        .collect(Collectors.toList())
        );
    }

    @SafeVarargs
    public static boolean populate(Map<String, Object> source, Map.Entry<String, Object>... params) {
        return populate(source, Arrays.asList(params));
    }

    public static boolean populate(Map<String, Object> source, Collection<Map.Entry<String, Object>> params) {
        boolean updated = false;

        for (Map.Entry<String, Object> param : params) {
            String path = param.getKey();
            Object newVal = param.getValue();

            Object tmpMap = source;
            Object tmpVal = null;
            String fieldName = "";
            Queue<String> tokens = new LinkedList<>(Arrays.asList(path.split("\\.")));
            while (tokens.peek() != null) {
                fieldName = tokens.remove();
                try {
                    tmpVal = ((Map) tmpMap).get(fieldName);
                    if (tmpVal instanceof Map) {
                        tmpMap = tmpVal;
                    } else {
                        break;
                    }
                } catch (Exception ignored) {
                }
            }

            if (!Objects.equals(tmpVal, newVal)) {
                LOGGER.debug("Updating value of [{}]: {} -> {}", path, tmpVal, newVal);
                ((Map) tmpMap).put(fieldName, newVal);

                if (!updated) {
                    updated = true;
                }
            }
        }

        return updated;
    }
}
