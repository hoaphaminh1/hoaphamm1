package vn.hoapm.util;

import java.util.*;
import java.util.concurrent.Callable;

/**
 */
public class SafeValue {

    public static final String EMPTY_STRING = "";
    public static final boolean DEFAULT_BOOLEAN = false;
    public static final int DEFAULT_INTEGER = 0;
    public static final long DEFAULT_LONG = 0L;
    public static final double DEFAULT_DOUBLE = 0.0d;
    public static final List EMPTY_LIST = new ArrayList();
    public static final Set EMPTY_SET = new LinkedHashSet();
    public static final Map EMPTY_MAP = new LinkedHashMap();


    private SafeValue() {
    }

    public static String ofString(Callable<String> supplier) {
        return of(supplier, EMPTY_STRING);
    }

    public static String asString(Callable<Object> supplier) {
        return ofString(() -> supplier.call().toString());
    }

    public static boolean ofBoolean(Callable<Boolean> supplier) {
        return of(supplier, DEFAULT_BOOLEAN);
    }

    public static boolean asBoolean(Callable<Object> supplier) {
        return ofBoolean(() -> Boolean.parseBoolean(supplier.call().toString()));
    }

    public static int ofInt(Callable<Integer> supplier) {
        return of(supplier, DEFAULT_INTEGER);
    }

    public static int asInt(Callable<Object> supplier) {
        return ofInt(() -> Integer.parseInt(supplier.call().toString()));
    }

    public static long ofLong(Callable<Long> supplier) {
        return of(supplier, DEFAULT_LONG);
    }

    public static double ofDouble(Callable<Double> supplier) {
        return of(supplier, DEFAULT_DOUBLE);
    }

    public static <O> List<O> ofList(Callable<List<O>> supplier) {
        return of(supplier, EMPTY_LIST);
    }

    public static <O> Set<O> ofSet(Callable<Set<O>> supplier) {
        return of(supplier, EMPTY_SET);
    }

    public static <K, V> Map<K, V> ofMap(Callable<Map<K, V>> supplier) {
        return of(supplier, EMPTY_MAP);
    }

    private static <T> T of(Callable<T> supplier, T defaultValue) {
        Objects.requireNonNull(defaultValue);

        T result = null;
        try {
            result = supplier.call();
        } catch (Exception e) {// ignored
        }

        return Objects.nonNull(result) ? result : defaultValue;
    }

}
