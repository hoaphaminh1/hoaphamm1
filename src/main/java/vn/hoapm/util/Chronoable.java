package vn.hoapm.util;

import vn.hoapm.util.functional.Maybe;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public interface Chronoable {
    Pattern WEEKS_DAYS_HOURS_PATTERN = Pattern.compile("^(?:(\\d+?)w)?(?:(\\d+?)d)?(?:(\\d+?)h)?$");
    Pattern HOURS_MINUTES_SECONDS_PATTERN = Pattern.compile("^(\\d\\d):(\\d\\d):(\\d\\d)$");

    long toSeconds();

    static Maybe<Chronoable> hoursMinutesSeconds(String string) {
        Matcher m = HOURS_MINUTES_SECONDS_PATTERN.matcher(string);
        if (m.matches()) {
            return Maybe.something(new HoursMinutesSeconds(
                Integer.parseInt(m.group(1)),
                Integer.parseInt(m.group(2)),
                Integer.parseInt(m.group(3))
            ));
        }
        return Maybe.nothing();
    }

    static Maybe<Chronoable> weeksDaysHours(String string) {
        Matcher m = WEEKS_DAYS_HOURS_PATTERN.matcher(string);
        if (m.matches()) {
            return Maybe.something(new WeeksDaysHours(
                Maybe.of(m.group(1)).map(Integer::parseInt).orElse(0),
                Maybe.of(m.group(2)).map(Integer::parseInt).orElse(0),
                Maybe.of(m.group(3)).map(Integer::parseInt).orElse(0)
            ));
        }
        return Maybe.nothing();
    }

    static Chronoable never() {
        return Never.INSTANCE;
    }

    static Chronoable localDateTimeDiff(LocalDateTime before, LocalDateTime after) {
        return new LocalDateTimeDiff(before, after);
    }

    default boolean isBefore(Chronoable chronoable) {
        return Maybe.of(chronoable)
            .map(other -> toSeconds() < other.toSeconds())
            .orElse(Boolean.TRUE);
    }

    class WeeksDaysHours implements Chronoable {
        private final int weeks;
        private final int days;
        private final int hours;

        private WeeksDaysHours(int weeks, int days, int hours) {
            this.weeks = weeks;
            this.days = days;
            this.hours = hours;
        }

        @Override
        public long toSeconds() {
            return TimeUnit.DAYS.toSeconds(weeks * 7 + days) + TimeUnit.HOURS.toSeconds(hours);
        }

        @Override
        public String toString() {
            StringBuilder result = new StringBuilder(10);
            if (weeks > 0) {
                result.append(weeks).append('w');
            }
            if (days > 0) {
                result.append(days).append('d');
            }
            if (hours > 0) {
                result.append(hours).append('h');
            }
            return result.toString();
        }
    }

    class HoursMinutesSeconds implements Chronoable {
        private final int hours;
        private final int minutes;
        private final int seconds;

        private HoursMinutesSeconds(int hours, int minutes, int seconds) {
            this.hours = hours;
            this.minutes = minutes;
            this.seconds = seconds;
        }

        @Override
        public long toSeconds() {
            return TimeUnit.HOURS.toSeconds(hours) + TimeUnit.MINUTES.toSeconds(minutes) + seconds;
        }

        @Override
        public String toString() {
            return String.format("%02d:%02d:%02d", hours, minutes, seconds);
        }
    }

    class LocalDateTimeDiff implements Chronoable {
        public static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("EEE MMM dd HH:mm:ss yyyy");

        private final LocalDateTime stamp;
        private final LocalDateTime now;

        public LocalDateTimeDiff(LocalDateTime stamp, LocalDateTime now) {
            this.stamp = stamp;
            this.now = now;
        }

        @Override
        public long toSeconds() {
            return ChronoUnit.SECONDS.between(stamp, now);
        }

        @Override
        public String toString() {
            return FORMATTER.format(stamp);
        }
    }

    enum Never implements Chronoable {
        INSTANCE;

        @Override
        public long toSeconds() {
            return Integer.MAX_VALUE;
        }

        @Override
        public String toString() {
            return "Never";
        }
    }
}
