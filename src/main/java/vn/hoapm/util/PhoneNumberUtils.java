package vn.hoapm.util;

import org.apache.commons.lang3.StringUtils;

/**
 */
public class PhoneNumberUtils {

    public static String formatE164(String _countryCode, String _areaCode, String _phoneNo) {
        String countryCode = _countryCode;
        String areaCode = StringUtils.trimToEmpty(_areaCode);
        String phoneNo = StringUtils.trimToEmpty(_phoneNo);

        return StringUtils.deleteWhitespace(StringUtils.join(
                StringUtils.startsWith(countryCode, "+") ? countryCode : ("+" + countryCode),
                StringUtils.startsWith(areaCode, "0") ? StringUtils.substring(areaCode, 1) : areaCode,
                phoneNo
        ));
    }

    public static String formatE164(String _countryCode, String _localFullNumber) {
        String countryCode = _countryCode;
        String localFullNumber = StringUtils.trimToEmpty(_localFullNumber);

        return StringUtils.deleteWhitespace(StringUtils.join(
                StringUtils.startsWith(countryCode, "+") ? countryCode : ("+" + countryCode),
                StringUtils.startsWith(localFullNumber, "0") ? StringUtils.substring(localFullNumber, 1) : localFullNumber
        ));
    }

    public static String formatE164WithoutPlusSign(String _countryCode, String _localFullNumber) {
        String countryCode = _countryCode;
        String localFullNumber = StringUtils.trimToEmpty(_localFullNumber);

        return StringUtils.deleteWhitespace(StringUtils.join(
                StringUtils.startsWith(countryCode, "+") ? StringUtils.substring(countryCode, 1) : ("" + countryCode),
                StringUtils.startsWith(localFullNumber, "0") ? StringUtils.substring(localFullNumber, 1) : localFullNumber
        ));
    }

}
