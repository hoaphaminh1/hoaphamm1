
package vn.hoapm.util;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;

/**
 */
public class DoubleValue {

    static {
        new DoubleValue();
    }

    /**
     * Format Double values
     * 
     * @param d
     * @return
     */
    public static final String format(double d) {
        /** Format amount **/
        DecimalFormat fmtObj = new DecimalFormat("0.00");
        fmtObj.setRoundingMode(RoundingMode.FLOOR);
        String f = fmtObj.format(d);
        return f;
    }
    public static final String formatAsInteger(double d) {
        DecimalFormat fmtObj = new DecimalFormat("0.#");
        fmtObj.setRoundingMode(RoundingMode.FLOOR);
        String f = fmtObj.format(d);
        return f;
    }

    public static double round(double input, int decimalPlace, RoundingMode roundingMode) {
        BigDecimal bigDecimal = BigDecimal.valueOf(input).setScale(decimalPlace, roundingMode);
        return bigDecimal.doubleValue();
    }
}
