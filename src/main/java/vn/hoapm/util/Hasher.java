
package vn.hoapm.util;

import java.security.NoSuchAlgorithmException;

/**
 * This provides an interface for obects capable of generating a hash-encoded version of a String. This can be passed to objects such as @
 * PasswordGenerator} to perform their hashing. The actual algorithm is left to the implementor.
 *
 */
public interface Hasher {

    /**
     * Returns a hash-encoded version of the string provided as parameter 'cleartext'.
     *
     * @param cleartext
     *            a cleartext string
     * @return String a hash of the cleartext string
     * @throws Exception
     *             if Calculation Error Occurs
     */
    String toHash(String cleartext) throws NoSuchAlgorithmException;
}
