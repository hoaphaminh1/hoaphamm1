
package vn.hoapm.util;


import java.util.*;

/**
 */
public class ListUtil<T> {

    /**
     * <p>
     * This method find intersection between two lists.
     * </p>
     * <p>
     * The method received two Lists. <b>original</b> contains all customer's related objects, and <b>edit</b> contains the modified objects we need
     * to modify. The returned List <b>result1</b> contains the customer's object which need to be disabled, and the List <b>result2</b> contains
     * objects need to be modified
     * </p>
     * 
     * @param original
     *            {@code List}
     * @param edit
     *            {@code List}
     * @param result1
     *            {@code List}
     * @param result2
     *            {@code List}
     */
    public void getIntersection(List<T> original, List<T> edit, List<T> result1, List<T> result2) {
        List<T> temp = new ArrayList<>();
        temp.addAll(original);

        result1.addAll(original);
        result1.removeAll(edit);

        result2.addAll(edit);
        result2.removeAll(temp);
    }

    /**
     * <p>
     * This method removed duplicate entries in a List.
     * </p>
     * 
     * @param arrayList
     *            {@code List}
     */

    public void removeDuplicate(List<T> arrayList) {
        Set<T> hashSet = new HashSet<>(arrayList);
        arrayList.clear();
        arrayList.addAll(hashSet);
    }

    public List<T> getDuplicate(Collection<T> list) {
        final List<T> duplicatedObjects = new ArrayList<>();
        Set<T> cache = new HashSet<>();
        for (T elem: list) {
            if (cache.contains(elem)) {
                duplicatedObjects.add(elem);
            } else {
                cache.add(elem);
            }
        }
        return duplicatedObjects;
    }
}
