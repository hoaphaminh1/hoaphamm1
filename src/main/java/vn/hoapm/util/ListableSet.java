
package vn.hoapm.util;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * A specialised Set implementation which can generate a comma separated list of its content object's names.
 *
 *
 * @param <T>
 *            any class which extends ListableByName
 */
@SuppressWarnings("serial")
public class ListableSet<T extends ListableByName> extends HashSet<T> implements Set<T> {


    /**
     * Iterates through all items in the set, extracts each name and generates a comma separated list of names as a String.
     *
     * @return a comma separated list of all element names
     */
    public final String getNameList() {
        String str = "";

        Iterator<T> i = this.iterator();

        // build string
        while (i.hasNext()) {
            str = str.concat(i.next().getName()).concat(",");
        }

        // strip trailing comma
        str = str.substring(0, (str.length() + -1));

        return str;
    }

}
