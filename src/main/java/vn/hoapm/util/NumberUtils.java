
package vn.hoapm.util;

import org.apache.commons.lang.StringUtils;
import vn.hoapm.util.functional.Try;

import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;


/**
 *
 */
public class NumberUtils {

    /**
     * Check an String is not blank and be able to parsed to integer value.
     */
    public static boolean isValidIntValue(String str) {
        if (StringUtils.isBlank(str)) {
            return false;
        }

        try {
            @SuppressWarnings("unused")
            int value = Integer.parseInt(str);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Check an String is not blank and be able to parsed to integer value that is >= 0.
     */
    public static boolean isPositiveIntValue(String str) {
        if (StringUtils.isBlank(str)) {
            return false;
        }

        try {
            int value = Integer.parseInt(str);
            return value >= 0 ;
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean isPositiveIntValue(Integer value) {
        return value != null && value > 0;
    }


    /**
     * Check an String is not blank and be able to parsed to long value.
     */
    public static boolean isValidLongValue(String str) {

        if (StringUtils.isBlank(str)) {
            return false;
        }

        try {
            Long.parseLong(str);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Check an String is not blank and be able to parsed to long value that is >= 0.
     */
    public static boolean isPositiveLongValue(String str) {

        if (StringUtils.isBlank(str)) {
            return false;
        }

        try {
            long longVal = Long.parseLong(str);
            if (longVal >= 0) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean isPositiveLongValueNullSafe(String str) {

        if (StringUtils.isBlank(str)) {
            return true;
        }

        try {

            long longVal = Long.parseLong(str);

            if (longVal >= 0) {

                return true;
            } else {

                return false;
            }
        } catch (Exception e) {
            return false;
        }
    }

    public static long parseLongQuietly(String str) {
        try {
            return Long.valueOf(str);
        } catch (Exception e) {
            return 0;
        }
    }

    public static List<Long> parseListStringToLong(List<String> strList) {

        List<Long> ids = new ArrayList<>();

        if (null == strList) {
            return ids;
        }

        for (String str : strList) {
            ids.add(parseLongQuietly(str));
        }

        return ids;
    }


    /**
     * Help to add leading Zero to an Integer. EX: 0 would be 001, 10 would be 0010.
     * 
     * @param value
     * @param length
     * @return
     */
    public static String addLeadingZerosToNumber(int value, int length) {
        if (value < 0 || length <= 0) {
            return CommonConstant.EMPTY;
        }

        if (String.valueOf(value).length() > length) { // Prevent for case: length of 9999 > 3
            return CommonConstant.EMPTY;
        }

        String formatted = String.format("%0" + length + "d", value);
        return formatted;
    }
    
    /**
     *  return number of digits after decimal point of a double value
     * @param value
     * @return
     */
    public static int getLengthOfFractionalPart(String value) {
        return RegexUtils.getLengthOfFractionalPart(value);
    }

    public static double roundHalfUp2Digit(double input) {
        return DoubleValue.round(input, CommonConstant.DAILY_OVERAGE_DECIMAL_PLACE, RoundingMode.HALF_UP);
    }

    public static double roundHalfUp(double input, int scale) {
        return DoubleValue.round(input, scale, RoundingMode.HALF_UP);
    }

    /**
     * Validate a text represent a positive double value
     * @param value
     * @return true if given value is a positive double value
     */
    public static boolean isPositiveDouble(String value, int maxFractionPartLength){
        return isValidDouble(value, maxFractionPartLength, doubleValue -> doubleValue > 0);
    }


    /**
     * check a given string is a non negative double value
     *
     * @param value string represent a non negative double value
     * @return true when given string value is 0 or positive double value
     */
    public static boolean isNonNegativeDouble(String value, int maxFractionPartLength){
        return isValidDouble(value, maxFractionPartLength, doubleValue -> doubleValue >= 0);
    }

    /**
     * check a double with exact fraction part exclude 0
     * for example with maxFractionPartLength = 2
     * 2.13 -> true
     * 1.1000 -> true
     * 3.141 -> false
     * @param value
     * @param maxFractionPartLength
     *
     * @return
     */
    private static boolean isValidFractionPart(String value, int maxFractionPartLength) {
        String fractionPart = RegexUtils.getFractionPart(value);
        int length = fractionPart.length();
        if (length <= maxFractionPartLength) {
            return true;
        }

        for (int i = maxFractionPartLength; i < length; i++) {
            if (fractionPart.charAt(i) != '0') {
                return false;
            }
        }
        return true;
    }

    /**
     * check a value is a valid double value with max fraction part and predicates
     * @param value
     * @param withMaxFractionPart
     * @param predicates
     * @return
     */
    public static boolean isValidDouble(String value, int withMaxFractionPart, Predicate<Double>... predicates) {
        Double doubleValue = Try.of(() -> Double.valueOf(value)).orElse(null);
        if (doubleValue == null) {
            return false;
        }
        if (predicates != null) {
            for (Predicate predicate : predicates) {
                if (!predicate.test(doubleValue)) {
                    return false;
                }
            }
        }
        return isValidFractionPart(value, withMaxFractionPart);
    }


}
