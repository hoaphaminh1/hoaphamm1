package vn.hoapm.util;

import java.util.Comparator;


/**
 *
 * Sorting strings in alphabetically order and ASCII values
 *
 */
public class AlphabeticalStringComparator implements Comparator<String> {
    @Override
    public int compare(String str1, String str2) {
        char[] first  = str1.toCharArray();
        char[] second = str2.toCharArray();
        int minLength = Math.min(first.length, second.length);
        for (int i = 0; i < minLength; i++) {
            int numericValFirst = Character.codePointAt(str1, i);
            int numericValSecond = Character.codePointAt(str2, i);
            if (numericValFirst == numericValSecond) continue;
            if (Character.toUpperCase(first[i]) == Character.toUpperCase(second[i]) ||
                    !(Character.isAlphabetic(numericValFirst) && Character.isAlphabetic(numericValSecond))) {
                return numericValFirst - numericValSecond;
            }
            else {
                char upperCaseVal1 = Character.toUpperCase(first[i]);
                char upperCaseVal2 = Character.toUpperCase(second[i]);
                return upperCaseVal1 - upperCaseVal2;
            }
        }
        return first.length - second.length;
    }
}
