package vn.hoapm.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.EnumSet;
import java.util.Map;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

public final class ConfigUtils {
    private ConfigUtils() {
        throw new IllegalStateException("Cannot initialize");
    }

    private static final Logger LOGGER = LoggerFactory.getLogger(ConfigUtils.class);

    public static void setConfigDataSource(Map<String, String> configs, EnumSet enumSet, Consumer<Map<String, String>> setConfigCallback) {
        Map<String, String> properties = (Map<String, String>) enumSet
                .stream().map(Objects::toString)
                .filter(configs::containsKey)
                .collect(Collectors.toMap(Function.identity(), configs::get));
        if (CollectionUtils.isNotEmpty(properties)) {
            if (LOGGER.isDebugEnabled()) {
                Class<?> firstElementClass = enumSet.iterator().next().getClass();
                properties.forEach((property, value) -> LOGGER.trace(">>> Setting [{}] datasource config [{}] with value [{}]",
                        firstElementClass.getSimpleName(),
                        property,
                        PatternStringUtils.mask(value)));
            }
            setConfigCallback.accept(properties);
        }

    }
}
