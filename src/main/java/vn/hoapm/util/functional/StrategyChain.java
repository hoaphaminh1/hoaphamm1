package vn.hoapm.util.functional;

import lombok.Builder;
import lombok.NonNull;

import java.util.Objects;
import java.util.Optional;
import java.util.function.*;

/**
 * A pipeline based construct for testing multiple strategies iteratively
 * @param <D> The type of the Data Source
 * @param <S> The type of the Strategy
 * @param <I> The intermediate type that will apply the Strategy to the data
 * @param <R> The return type of the application of the Strategy
 */
public class StrategyChain<D, S, I, R> {
    private final S strategy;
    private final R result;
    private final D dataSource;
    private final BiFunction<D, S, I> strategyProvider;
    private final Function<I, R> strategyApplicator;
    private final Predicate<R> successTest;

    @Builder
    private StrategyChain(D dataSource, BiFunction<D, S, I> strategyProvider, Function<I, R> strategyApplicator, Predicate<R> successTest) {
        this(null, null, dataSource, strategyProvider, strategyApplicator, successTest);
    }

    private StrategyChain(S strategy, R result, @NonNull D dataSource, @NonNull BiFunction<D, S, I> strategyProvider, @NonNull Function<I, R> strategyApplicator, @NonNull Predicate<R> successTest) {
        this.strategy = strategy;
        this.result = result;
        this.dataSource = dataSource;
        this.strategyProvider = strategyProvider;
        this.strategyApplicator = strategyApplicator;
        this.successTest = successTest;
    }

    /**
     * If the result has not been evaluated, or was not successful, evaluate again with the supplied strategy
     * @param strategySupplier A supplier that will provide a strategy to evaluate.
     * @return a new StrategyChain if the result was not a success.
     */
    public StrategyChain<D, S, I, R> chain(Supplier<S> strategySupplier) {
        Objects.requireNonNull(strategySupplier, "strategySupplier null");
        if (Objects.isNull(result) || !successTest.test(result)) {
            final S strategy = strategySupplier.get();
            final R result = strategyApplicator.apply(strategyProvider.apply(dataSource, strategy));
            return new StrategyChain<>(strategy, result, dataSource, strategyProvider, strategyApplicator, successTest);
        }
        return this;
    }

    private R assertResultEvaluated() {
        if (Objects.isNull(result)) {
            throw new IllegalStateException("Not Evaluated Yet");
        }
        return result;
    }

    private boolean isSuccess() {
        return successTest.test(assertResultEvaluated());
    }

    public Optional<R> getResult() {
        return isSuccess() ? Optional.of(result) : Optional.empty();
    }

    /**
     * Internal API for examining the strategy of a successful result
     * @return Option of strategy if success; else empty.
     */
    Optional<S> getStrategy() {
        return isSuccess() ? Optional.of(strategy) : Optional.empty();
    }

    public boolean ifFail(Consumer<D> toRun) {
        Objects.requireNonNull(toRun, "toRun null");
        final boolean successful = isSuccess();
        if (!successful) {
            toRun.accept(dataSource);
        }
        return successful;
    }
}
