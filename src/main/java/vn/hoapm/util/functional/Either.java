package vn.hoapm.util.functional;



import vn.hoapm.util.functional.higher.Functor;
import vn.hoapm.util.functional.higher.Functor2;
import vn.hoapm.util.functional.higher.Peek;
import vn.hoapm.util.functional.higher.Peek2;

import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Function;

public interface Either<L, R> extends Functor2<Either, L, R>, Peek2<L, R> {

    boolean isLeft();

    default boolean isRight() {
        return !isLeft();
    }

    L left();
    R right();

    default <U1, U2> Either<U1, U2> map(Function<? super L, ? extends U1> ifLeft, Function<? super R, ? extends U2> ifRight) {
        Objects.requireNonNull(ifLeft);
        Objects.requireNonNull(ifRight);
        return isLeft() ? Either.left(ifLeft.apply(left())) : Either.right(ifRight.apply(right()));
    }

    @SuppressWarnings("unchecked")
    default <U1, U2> Either<U1, U2> flatMap(Function<? super L, ? extends Either<? extends U1, ? extends U2>> ifLeft, Function<? super R, ? extends Either<? extends U1, ? extends U2>> ifRight) {
        Objects.requireNonNull(ifLeft);
        Objects.requireNonNull(ifRight);
        return isLeft() ? (Either<U1, U2>) ifLeft.apply(left()) : (Either<U1, U2>) ifRight.apply(right());
    }

    default void peek(Consumer<? super L> ifLeft, Consumer<? super R> ifRight) {
        Objects.requireNonNull(ifRight);
        Objects.requireNonNull(ifLeft);
        if (isLeft()) {
            ifLeft.accept(left());
        } else {
            ifRight.accept(right());
        }
    }

    default LeftProjection<L, R> leftProject() {
        return new LeftProjection<>(this);
    }

    default RightProjection<L, R> rightProject() {
        return new RightProjection<>(this);
    }

    static <L, R> Left<L, R> left(L left) {
        return new Left<>(left);
    }

    static <L, R> Right<L, R> right(R right) {
        return new Right<>(right);
    }

    interface Projection<L, R> {
        Either<L, R> restore();
    }

    class LeftProjection<L, R> implements Projection<L, R>, Functor<LeftProjection, L>, Peek<L> {

        private final Either<L, R> value;

        @Override
        public Either<L, R> restore() {
            return value;
        }

        private LeftProjection(Either<L, R> value) {
            this.value = value;
        }

        @Override
        public <U> LeftProjection<U, R> map(Function<? super L, ? extends U> mapper) {
            Objects.requireNonNull(mapper);
            return restore().isLeft() ? new LeftProjection<>(new Left<>(mapper.apply(restore().left()))) : Functor.Type.<LeftProjection<U, R>, LeftProjection, U>castParam(this);
        }

        @Override
        public void peek(Consumer<? super L> consumer) {
            Objects.requireNonNull(consumer);
            if (restore().isLeft()) {
                consumer.accept(restore().left());
            }
        }

        public <X extends Exception> void throwLeft(Function<? super L, ? extends X> exceptionMapper) throws X {
            Objects.requireNonNull(exceptionMapper);
            if (restore().isLeft()) {
                throw exceptionMapper.apply(restore().left());
            }
        }
    }

    class RightProjection<L, R> implements Projection<L, R>, Functor<RightProjection, R>, Peek<R> {

        private final Either<L, R> value;

        private RightProjection(Either<L, R> value) {
            this.value = value;
        }

        @Override
        public Either<L, R> restore() {
            return value;
        }

        @Override
        public <U> RightProjection<L, U> map(Function<? super R, ? extends U> mapper) {
            Objects.requireNonNull(mapper);
            return !value.isLeft() ? new RightProjection<>(new Right<>(mapper.apply(restore().right()))) : Functor.Type.<RightProjection<L, U>, RightProjection, U>castParam(this);
        }

        @Override
        public void peek(Consumer<? super R> consumer) {
            Objects.requireNonNull(consumer);
            if (!value.isLeft()) {
                consumer.accept(restore().right());
            }
        }

        public Maybe<R> toMaybe(Consumer<? super L> ifLeft) {
            Objects.requireNonNull(ifLeft);
            if (value.isLeft()) {
                ifLeft.accept(restore().left());
                return Maybe.nothing();
            } else {
                return Maybe.of(restore().right());
            }
        }

        public R orElseGet(R ifLeft) {
            Objects.requireNonNull(ifLeft);
            if (value.isLeft()) {
                return ifLeft;
            } else {
                return restore().right();
            }
        }

        public <X extends Exception> R throwIfLeft(Function<? super L, ? extends X> mapper) throws X {
            Objects.requireNonNull(mapper);
            if (value.isLeft()) {
                throw mapper.apply(restore().left());
            } else {
                return restore().right();
            }
        }

        public <X extends Exception> void doOrElseThrow(Function<? super L, ? extends X> ifLeft, Consumer<? super R> ifRight) throws X {
            Objects.requireNonNull(ifLeft);
            Objects.requireNonNull(ifRight);
            if (value.isLeft()) {
                throw ifLeft.apply(restore().left());
            } else {
                ifRight.accept(restore().right());
            }
        }
    }

    class Left<L, R> implements Either<L, R> {

        private final L value;

        private Left(L value) {
            this.value = value;
        }

        @Override
        public boolean isLeft() {
            return true;
        }

        @Override
        public L left(){
            return value;
        }

        @Override
        public R right() throws NoSuchElementException {
            throw new NoSuchElementException("No Right element on Left kind.");
        }
    }

    class Right<L, R> implements Either<L, R> {

        private final R value;

        private Right(R value) {
            this.value = value;
        }

        @Override
        public boolean isLeft() {
            return false;
        }

        @Override
        public L left() throws NoSuchElementException {
            throw new NoSuchElementException("No Left element on Right kind.");
        }

        @Override
        public R right() {
            return value;
        }
    }
}
