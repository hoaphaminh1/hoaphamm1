package vn.hoapm.util.functional;

import java.util.function.Supplier;

public class ConditionalBranch {

    private boolean condition;

    private ConditionalBranch(boolean condition) {
        this.condition = condition;
    }

    public static ConditionalBranch checkIf(boolean condition) {
        return new ConditionalBranch(condition);
    }

    public <X extends Exception> ConditionalBranch thenDo(Executable supplier) throws X {
        try {
            if (this.condition) {
                supplier.execute();
            }

            return this;
        } catch (Exception e) {
            throw (X) e;
        }
    }

    public <X extends Exception> ConditionalBranch elseDo(Executable supplier) throws X {
        try {
            if (!this.condition) {
                supplier.execute();
            }

            return this;
        } catch (Exception e) {
            throw (X) e;
        }
    }

    public <X extends Exception> void throwException(Supplier<X> supplier) throws X {
        if (this.condition) {
            throw supplier.get();
        }
    }

    public static <X extends Exception> void doIf(boolean condition, Executable supplier) throws X {
        try {
            if (condition) {
                supplier.execute();
            }
        } catch (Exception e) {
            throw (X) e;
        }
    }
}
