package vn.hoapm.util.functional.collections;



import vn.hoapm.util.functional.higher.Monad;
import vn.hoapm.util.functional.higher.Monoid;

import java.util.HashSet;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collector;
import java.util.stream.Collectors;

/**
 * <p>Immutable Linked List that acts as an algebraic data type.</p>
 * <p>{@code List<E> => Cons(head: E, tail: List<E>) | Empty}</p>
 * @param <E> The type of the elements contained
 */
public interface Linked<E> extends Seq<Linked, E> {

    static <E> Linked<E> of(E elem) {
        return cons(elem, empty());
    }

    @SafeVarargs
    static <R> Linked<R> many(R... elems) {
        if (Objects.isNull(elems)) {
            return of(null);
        }
        Linked<R> cons = empty();
        for (int x = elems.length - 1; x >= 0; x--) {
            cons = cons.push(elems[x]);
        }
        return cons;
    }

    static <E> Linked<E> cons(E head, Linked<E> tail) {
        return new Cons<>(head, tail);
    }

    static <E> Linked<E> empty() {
        return Monoid.Type.<Linked<E>, Linked, E>castParam(EmptyLinked.EMPTY);
    }

    @Override
    default Linked<E> push(E elem) {
        return cons(elem, this);
    }

    @Override
    default Linked<E> reverse() {
        return foldLeft(identity(), Linked::push);
    }

    @Override
    default Linked<E> identity() {
        return Linked.empty();
    }

    @Override
    default Linked<E> mconcat(Linked<Monoid<Linked, ? extends E>> list) {
        return Monoid.Type.narrowHigher(Seq.super.mconcat(list));
    }

    default Linked<E> mappend(Monoid<Linked, ? extends E> other) {
        return foldRight(Monoid.Type.<Linked<E>, Linked, E>narrowHigher(other), Linked::push);
    }

    @Override
    default Linked<E> append(E elem) {
        return foldRight(Linked.<E>empty().push(elem), Linked::push);
    }

    @Override
    default <O> Linked<E> distinct(Function<E, O> propertyGetter) {
        HashSet<O> distinctProperties = new HashSet<>();
        Predicate<E> isDistinct = x -> {
            final O property = propertyGetter.apply(x);
            final boolean notFound = !distinctProperties.contains(property);
            if (notFound) {
                distinctProperties.add(property);
            }
            return notFound;
        };
        return foldRight(identity(), (xs, x) -> isDistinct.test(x) ? xs.push(x) : xs);
    }

    @Override
    Linked<E> tail();

    @Override
    default <U> Linked<U> map(Function<? super E, ? extends U> mapper) {
        Objects.requireNonNull(mapper);
        return !isEmpty() ? foldRight(Linked.empty(), (xs, x) -> xs.push(mapper.apply(x))) : empty();
    }

    @Override
    @SuppressWarnings("unchecked")
    default <U> Linked<U> flatMap(Function<? super E, Monad<Linked, ? extends U>> mapper) {
        Objects.requireNonNull(mapper);
        return !isEmpty() ? Linked.<U>empty().mconcat(map(mapper.andThen(Monad.Type::<Linked<U>, Linked, U>narrow))) : empty();
    }

    @Override
    default <U> Linked<U> pure(U value) {
        return of(value);
    }

    final class Stateful<U> {

        /**
         * <p>Produces a {@link Collector} that pushes elements onto to a {@link Linked} stack and then reversing it.</p>
         * <p>Is slower than {@link Stateful#stackCollector()} but preserves encounter order</p>
         * @implNote  O(2n) as stack is reversed to preserve order
         * @param <U> type of elements in this wrapped {@link Linked} list
         * @return a collector that will accumulate a {@link Linked} list
         */
        public static <U> Collector<U, Stateful<U>, Linked<U>> collector() {
            return Collector.of(Stateful::impure, Stateful::push, Stateful::append, s -> s.pure().reverse(), Collector.Characteristics.CONCURRENT);
        }

        /**
         * <p>Produces a {@link Collector} that pushes elements onto a {@link Linked} stack.</p>
         * <p>Is faster than {@link Stateful#collector()} when order is not important</p>
         * @implNote O(n) as stack is returned immediately
         * @param <U> type of elements in this wrapped {@link Linked} list
         * @return a collector that will accumulate a {@link Linked} list
         */
        public static <U> Collector<U, Stateful<U>, Linked<U>> stackCollector() {
            return Collector.of(Stateful::impure, Stateful::push, Stateful::append, Stateful::pure, Collector.Characteristics.CONCURRENT);
        }

        private Linked<U> state;

        public static <U> Stateful<U> impure() {
            return new Stateful<>();
        }

        private Stateful() {
            state = empty();
        }

        public Linked<U> pure() {
            return state;
        }

        public void push(U u) {
            state = state.push(u);
        }

        public Stateful<U> append(Stateful<U> other) {
            state = other.state.mappend(state);
            return this;
        }
    }

    final class Cons<E> implements Linked<E> {

        private final E head;
        private final Linked<E> tail;

        private Cons(E head, Linked<E> tail) {
            this.head = head;
            this.tail = tail;
        }

        @Override
        public E head() {
            return head;
        }

        @Override
        public Linked<E> tail() {
            return tail;
        }

        /**
         * Traverses the Sequence to calculate the length
         * @apiNote Operation is O(n);
         */
        @Override
        public int length() {
            return foldLeft(0, (count, x) -> count + 1);
        }

        /**
         * {@inheritDoc}
         * @apiNote Operation is O(1)
         */
        @Override
        public boolean isEmpty() {
            return false;
        }

        /**
         * {@inheritDoc}
         * @apiNote Operation is O(n)
         */
        @Override
        public E get(int i) throws IndexOutOfBoundsException {
            if (i < 0) {
                throw new IllegalArgumentException("Index must be positive");
            }
            int count = 0;
            Linked<E> elements = this;
            while (!elements.isEmpty()) {
                if (count == i) {
                    return elements.head();
                }
                count = count + 1;
                elements = elements.tail();
            }
            throw new IndexOutOfBoundsException("List is of length " + count + ". Index " + i + " is out of range");
        }

        @Override
        public <O> O foldLeft(O accumulator, BiFunction<O, E, O> mapper) {
            return fold(this, accumulator, mapper);
        }

        @Override
        public <O> O foldRight(O accumulator, BiFunction<O, E, O> mapper) {
            return fold(reverse(), accumulator, mapper);
        }

        private <O> O fold(Linked<E> elements, O accumulator, BiFunction<O, E, O> mapper) {
            Objects.requireNonNull(mapper);
            while (!elements.isEmpty()) {
                accumulator = mapper.apply(accumulator, elements.head());
                elements = elements.tail();
            }
            return accumulator;
        }

        @Override
        public int hashCode() {
            return foldLeft(1, (hashCode, elem) -> 31 * hashCode + Objects.hashCode(elem));
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            } else if (obj instanceof Seq) {
                Iterator<?> other = ((Seq) obj).iterator();
                for (Object o: this) {
                    if (!other.hasNext() || !Objects.equals(other.next(), o)) {
                        return false;
                    }
                }
                return !other.hasNext();
            }
            return false;
        }

        @Override
        public String toString() {
            return toJavaStream().map(String::valueOf).collect(Collectors.joining(", ", "[", "]"));
        }

        @Override
        public Iterator<E> iterator() {
            return new Iterator<E>() {

                private Linked<E> provider = Cons.this;

                @Override
                public boolean hasNext() {
                    return !provider.isEmpty();
                }

                @Override
                public E next() {
                    if (provider.isEmpty()) {
                        throw new NoSuchElementException();
                    }
                    E elem = provider.head();
                    provider = provider.tail();
                    return elem;
                }
            };
        }
    }

    final class EmptyLinked<E> implements Linked<E>, Empty<Linked, E> {

        private static final EmptyLinked<?> EMPTY = new EmptyLinked<>();

        @Override
        public E head() {
            throw new NoSuchElementException();
        }

        @Override
        public Linked<E> tail() {
            throw new NoSuchElementException();
        }

        @Override
        public Linked<E> identity() {
            return this;
        }

        @Override
        public Linked<E> reverse() {
            return this;
        }

        @Override
        public int hashCode() {
            return 31;
        }

        @Override
        public boolean equals(Object obj) {
            return obj == this || obj instanceof Linked.EmptyLinked;
        }

        @Override
        public String toString() {
            return "[]";
        }

        private EmptyLinked() {}
    }
}
