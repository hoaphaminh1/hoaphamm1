package vn.hoapm.util.functional.collections;


import vn.hoapm.util.functional.higher.Monad;
import vn.hoapm.util.functional.higher.Monoid;
import vn.hoapm.util.functional.higher.Peek;

import java.util.*;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public interface Seq<INSTANCE extends Seq, E> extends Foldable<E>, Monoid<INSTANCE, E>, Monad<INSTANCE, E>, Peek<E> {

    int length();
    boolean isEmpty();
    E get(int i);
    E head();
    Seq<INSTANCE, E> tail();
    Seq<INSTANCE, E> push(E elem);
    Seq<INSTANCE, E> append(E elem);
    Seq<INSTANCE, E> reverse();
    <O> Seq<INSTANCE, E> distinct(Function<E, O> propertyGetter);

    @Override
    default void peek(Consumer<? super E> consumer) {
        forEach(consumer);
    }

    default List<E> toJavaList() {
        return foldLeft(new ArrayList<>(), (xs, x) -> {
            xs.add(x);
            return xs;
        });
    }

    default Stream<E> toJavaStream() {
        return StreamSupport.stream(Spliterators.spliteratorUnknownSize(iterator(), Spliterator.ORDERED), false);
    }

    interface Empty<INSTANCE extends Seq, E> extends Seq<INSTANCE, E> {

        default int length() {
            return 0;
        }

        default boolean isEmpty() {
            return true;
        }

        default E get(int i) throws IndexOutOfBoundsException {
            if (i < 0) {
                throw new IllegalArgumentException("Index must be positive");
            }
            throw new IndexOutOfBoundsException("List is empty");
        }

        default <O> O foldLeft(O accumulator, BiFunction<O, E, O> mapper) {
            Objects.requireNonNull(mapper);
            return accumulator;
        }

        default <O> O foldRight(O accumulator, BiFunction<O, E, O> mapper) {
            Objects.requireNonNull(mapper);
            return accumulator;
        }

        default List<E> toJavaList() {
            return Collections.emptyList();
        }

        default Stream<E> toJavaStream() {
            return Stream.empty();
        }

        default Iterator<E> iterator() {
            return Collections.emptyIterator();
        }
    }
}
