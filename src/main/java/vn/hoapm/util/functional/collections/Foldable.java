package vn.hoapm.util.functional.collections;

import java.util.function.BiFunction;

public interface Foldable<E> extends Iterable<E> {
    /**
     * Accumulate a value starting from the left of a sequence;
     * @param accumulator
     * @param mapper
     * @param <O>
     * @return
     */
    <O> O foldLeft(O accumulator, BiFunction<O, E, O> mapper);

    /**
     * Accumulate a value starting from the right of a sequence;
     * @param accumulator
     * @param mapper
     * @param <O>
     * @return
     */
    <O> O foldRight(O accumulator, BiFunction<O, E, O> mapper);
}
