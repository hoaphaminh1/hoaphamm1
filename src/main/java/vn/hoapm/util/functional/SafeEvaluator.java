package vn.hoapm.util.functional;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.concurrent.Callable;

@FunctionalInterface
public interface SafeEvaluator {

    <E> Try<E> with(Callable<E> supplier);

    @Component
    class TransactReadOnly implements SafeEvaluator {
        @Transactional(readOnly = true, transactionManager = TransactionManager.READ_ONLY)
        public <E> Try<E> with(Callable<E> supplier) {
            return Try.of(supplier);
        }
    }

    @Component
    class Transact implements SafeEvaluator {
        @Transactional
        public <E> Try<E> with(Callable<E> supplier) {
            return Try.of(supplier);
        }
    }
}
