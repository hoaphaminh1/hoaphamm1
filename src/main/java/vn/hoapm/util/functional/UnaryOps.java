package vn.hoapm.util.functional;

import java.util.Objects;
import java.util.Optional;
import java.util.function.*;

public class UnaryOps {

    public static <X extends Exception> Function<Exception, X> castOrMap(Class<? extends X> type, BiFunction<String, Throwable, X> factory) {
        return e -> {
            if (type.isInstance(e)) {
                return type.cast(e);
            } else {
                return factory.apply(e.getMessage(), e.getCause());
            }
        };
    }

    public static <O> Predicate<O> equalTo(O value) {
        return value == null ? Objects::isNull : value::equals;
    }

    public static <I, O> Function<I, O> castIfInstance(Class<O> toCast) {
        Objects.requireNonNull(toCast);
        return i -> toCast.isInstance(i) ? toCast.cast(i) : null;
    }

    public static <I, O> Function<I, O> ifNull(Function<I, O> mapper, Consumer<I> ifNull) {
        return x -> {
            final O y = mapper.apply(x);
            if (y == null) {
                ifNull.accept(x);
            }
            return y;
        };
    }

    public static int toInt(boolean bool) {
        return bool ? 1 : 0;
    }

    public static <O> ToIntFunction<O> toInt(Predicate<O> test) {
        return x -> toInt(test.test(x));
    }

    public static <O> Function<Boolean, Optional<O>> supplyIfPresent(O value) {
        return cond -> cond != null && cond ? Optional.of(value) : Optional.empty();
    }

    /**
     * Maps a value using two functions depending on the result;
     * @param initial The function to apply if the input isn't null
     * @param ifEmpty The function to apply if the result of initial is null
     * @param <O> The input type of the input
     * @param <T> The output type of the composed Function
     * @return result of the first, or the second if null.
     * @throws NullPointerException if any parameter is null;
     */
    public static <O, T> Function<O, T> mapFirstElseSecond(Function<O, T> initial, Function<O, T> ifEmpty) {
        Objects.requireNonNull(initial);
        Objects.requireNonNull(ifEmpty);
        return x -> {
            T returnIfNotNull = initial.apply(Objects.requireNonNull(x));
            return Objects.isNull(returnIfNotNull) ? ifEmpty.apply(x) : returnIfNotNull;
        };
    }
}
