package vn.hoapm.util.functional;

@FunctionalInterface
public interface CheckedIterable<E> extends Iterable<E> {

    interface Consumer<E, X extends Exception> {
        void accept(E element) throws X;
    }

    default <X extends Exception> void forEachChecked(Consumer<? super E, ? extends X> consumer) throws X {
        for (E element: this) {
            consumer.accept(element);
        }
    }
}
