package vn.hoapm.util.functional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TryTo {

    private final SafeEvaluator.TransactReadOnly transactReadOnly;
    private final SafeEvaluator.Transact transact;

    @Autowired
    public TryTo(SafeEvaluator.TransactReadOnly transactReadOnly, SafeEvaluator.Transact transact) {
        this.transactReadOnly = transactReadOnly;
        this.transact = transact;
    }

    public SafeEvaluator.TransactReadOnly makeReadOnlyTransaction() {
        return transactReadOnly;
    }

    public SafeEvaluator.Transact makeTransaction() {
        return transact;
    }
}
