package vn.hoapm.util.functional;

import java.util.Iterator;
import java.util.Objects;
import java.util.function.BiFunction;
import java.util.function.BinaryOperator;

public class BinaryOps {
    public static BinaryOperator<String> join(String connector) {
        return (a, b) -> a + connector + b;
    }

    public static <E, O, T> Iterable<T> zip(Iterable<E> first, Iterable<O> second, BiFunction<E, O, T> mapper) {
        Objects.requireNonNull(first);
        Objects.requireNonNull(second);
        Objects.requireNonNull(mapper);
        return () -> new Iterator<T>() {

            Iterator<E> firstIt = first.iterator();
            Iterator<O> secondIt = second.iterator();

            @Override
            public boolean hasNext() {
                return firstIt.hasNext() && secondIt.hasNext();
            }

            @Override
            public T next() {
                return mapper.apply(firstIt.next(), secondIt.next());
            }
        };
    }
}
