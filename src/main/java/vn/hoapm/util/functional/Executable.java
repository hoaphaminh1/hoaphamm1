package vn.hoapm.util.functional;


@FunctionalInterface
public interface Executable {

    void execute() throws Exception;
}
