package vn.hoapm.util.functional;

public class TernaryOps {
    public interface Consume3<T, U, O> {
        void accept(T t, U u, O o);
    }
}
