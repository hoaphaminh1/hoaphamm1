package vn.hoapm.util.functional;


import vn.hoapm.util.functional.higher.Monad;
import vn.hoapm.util.functional.higher.Peek;
import vn.hoapm.util.functional.higher.Value;

import java.io.Serializable;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Optional;
import java.util.function.*;

/**
 * <p>Pure {@link Monad} alternative to {@link Optional}. Acts as a container for an element that may not be present.</p>
 * <ul>
 *     <li>Unlike {@link Optional} it is also intended to be used as a method argument and won't generate warnings</li>
 *     <li>It is also serializable unlike {@link Optional}</li>
 * </ul>
 * @param <E> the Type of the value contained when an instance of {@link Something}.
 */
public interface Maybe<E> extends Monad<Maybe, E>, Peek<E>, Value<Maybe, E> {

    boolean isSome();

    @Override
    default boolean isEmpty() {
        return !isSome();
    }

    /**
     * Constructs one of the subtypes of {@link Maybe}
     * @param value The value to wrap in {@link Maybe}
     * @param <O> The Type of the value
     * @return {@link Something} if the value is not null; otherwise {@link Nothing}
     */
    static <O> Maybe<O> of(O value) {
        return value == null ? nothing() : new Something<>(value);
    }

    static <O> Maybe<O> something(O value) {
        return new Something<>(Objects.requireNonNull(value));
    }

    static <I, O> Function<? super I, Monad<Maybe, ? extends O>> lift(Function<? super I, ? extends O> func) {
        Objects.requireNonNull(func, "func");
        return x -> Maybe.of(func.apply(x));
    }

    /**
     * Constructs {@link Nothing}
     * @param <O> The parameter type expected for this {@link Maybe}
     * @return {@link Nothing}
     */
    static <O> Maybe<O> nothing() {
        return Nothing.cast(Nothing.NOTHING);
    }

    default Maybe<E> or(Supplier<? extends Value<Maybe, ? extends E>> alternative) {
        Objects.requireNonNull(alternative);
        return isSome() ? this : Value.Type.<Maybe<E>, Maybe, E>narrow(alternative.get());
    }

    @Override
    default Maybe<E> orUse(Value<Maybe, ? extends E> alternative) {
        Objects.requireNonNull(alternative);
        return isSome() ? this : Value.Type.<Maybe<E>, Maybe, E>narrow(alternative);
    }

    @Override
    default Maybe<E> orLift(Supplier<? extends E> alternative) {
        Objects.requireNonNull(alternative);
        return isSome() ? this : Maybe.of(alternative.get());
    }

    @Override
    default <U> Maybe<U> pure(U value) {
        return of(value);
    }

    @Override
    default void peek(Consumer<? super E> ifSome) {
        Objects.requireNonNull(ifSome);
        if (isSome()) {
            ifSome.accept(get());
        }
    }

    default Maybe<E> filter(Predicate<? super E> test) {
        return isSome() && test.test(get()) ? this : nothing();
    }

    default Maybe<E> compare(Maybe<E> other, BiPredicate<? super E, ? super E> test) {
        Objects.requireNonNull(other, "other");
        Objects.requireNonNull(test, "test");
        return
            map(value ->
                other.map(otherValue -> test.test(value, otherValue) ? value : otherValue)
                     .orElse(value)
            )
            .orUse(other);
    }

    default <O> Maybe<O> map(Function<? super E, ? extends O> ifSome) {
        Objects.requireNonNull(ifSome);
        return isSome() ? new Something<>(ifSome.apply(get())) : Nothing.cast(this);
    }

    default <O> Maybe<O> flatMap(Function<? super E, Monad<Maybe, ? extends O>> ifSome) {
        Objects.requireNonNull(ifSome);
        return isSome() ? Objects.requireNonNull(Monad.Type.<Maybe<O>, Maybe, O>narrow(ifSome.apply(get()))) : Nothing.cast(this);
    }

    default <O> Try<Maybe<O>> tryFlatMap(Try.CheckedFunction<? super E, Monad<Maybe, ? extends O>> ifSome) {
        Objects.requireNonNull(ifSome);
        return isSome()
            ? Try.of(() -> Monad.Type.<Maybe<O>, Maybe, O>narrow(ifSome.apply(get())))
            : Try.success(Nothing.cast(this));
    }

    /**
     * {@link Something} is a simple container that always contains a non Null value when constructed using the API: {@link Maybe#of(Object)}
     * @param <E> the Type of the value contained within.
     */
    class Something<E> implements Maybe<E>, Serializable {
        private static final long serialVersionUID = 6246407457511388202L;
        private final E value;

        @Override
        public boolean isSome() {
            return true;
        }

        @Override
        public E get() {
            return value;
        }

        @Override
        public int hashCode() {
            return Objects.hashCode(get());
        }

        @Override
        public boolean equals(Object obj) {
            return obj == this || obj instanceof Something && Objects.equals(get(), ((Something) obj).get());
        }

        @Override
        public String toString() {
            return "Something(" + get() + ")";
        }

        private Something(E value) {
            this.value = value;
        }
    }

    /**
     * {@link Nothing} is always empty.
     * @param <E> The type that would be contained if this was an instance of {@link Something}
     */
    class Nothing<E> implements Maybe<E>, Serializable {
        private static final long serialVersionUID = 1427941316346618464L;
        private static final Nothing<?> NOTHING = new Nothing<>();

        @Override
        public boolean isSome() {
            return false;
        }

        @Override
        public E get() {
            throw new NoSuchElementException();
        }

        @SuppressWarnings("unchecked")
        private static <O, E> Nothing<O> cast(Maybe<E> maybe) {
            return (Nothing<O>) maybe;
        }

        @Override
        public int hashCode() {
            return 31;
        }

        @Override
        public boolean equals(Object obj) {
            return obj == this || obj instanceof Nothing;
        }

        @Override
        public String toString() {
            return "Nothing()";
        }

        private Nothing() {
        }
    }
}
