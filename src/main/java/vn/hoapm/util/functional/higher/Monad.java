package vn.hoapm.util.functional.higher;

import java.util.Objects;
import java.util.function.BiFunction;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.UnaryOperator;

/**
 * Type Class that encodes a sequence of operations over a constant INSTANCE, where its Type parameter, T, may change.
 * {@link Monad} extends {@link Functor} because it allows for context changing computations over subclasses of INSTANCE
 * @param <INSTANCE> The subclass instance of {@link Monad} that may change between subclasses during operations
 * @param <T> the mutable type parameter
 */
public interface Monad<INSTANCE extends Monad, T> extends Functor<INSTANCE, T> {

    /**
     * Creates a new {@link Monad} of arbitrary type parameters
     */
    <U> Monad<INSTANCE, U> pure(U value);

    /**
     * Map over the type parameter. May change context between subtypes of INSTANCE
     */
    <U> Monad<INSTANCE, U> flatMap(Function<? super T, Monad<INSTANCE, ? extends U>> mapper);

    /**
     * Lift a unary operator to a monadic version over INSTANCE
     * @param operator the operator to lift
     * @param <U> the type parameter of the input operator
     * @param <INSTANCE> the Monad instance
     * @param <FINAL> the narrowed type of the Monad
     * @return a unary operator that respects monadic context
     */
    static <U, INSTANCE extends Monad, FINAL extends Monad<INSTANCE, U>> UnaryOperator<FINAL> m1Op(UnaryOperator<U> operator) {
        Objects.requireNonNull(operator);
        return x -> Type.narrow(x.flatMap(a -> x.pure(operator.apply(a))));
    }

    /**
     * Lift a binary operator to a monadic version over INSTANCE
     * @param operator the operator to lift
     * @param <U> the type parameter of the input operator
     * @param <INSTANCE> the Monad instance
     * @param <FINAL> the narrowed type of the Monad
     * @return a binary operator that respects monadic context
     */
    static <U, INSTANCE extends Monad, FINAL extends Monad<INSTANCE, U>> BinaryOperator<FINAL> m2Op(BinaryOperator<U> operator) {
        Objects.requireNonNull(operator);
        return (x, y) -> Type.narrow(x.flatMap(a -> y.flatMap(b -> y.pure(operator.apply(a, b)))));
    }

    /**
     * Lift a function mapping one type to another into monadic context over INSTANCE
     * @param function the function to lift
     * @param <X> the input type of the input function
     * @param <U> the return type of the input function
     * @param <INSTANCE> the Monad instance
     * @param <XM> the narrowed type of the returned function's input Monad
     * @param <UM> the narrowed type of the returned function's output Monad
     * @return a function X -> U in monadic context
     */
    static <X, U, INSTANCE extends Monad, XM extends Monad<INSTANCE, X>, UM extends Monad<INSTANCE, U>> Function<XM, UM> m1(Function<? super X, ? extends U> function) {
        Objects.requireNonNull(function);
        return x -> Type.narrow(x.flatMap(a -> x.pure((U)function.apply(a))));
    }

    /**
     * Lift a binary function mapping two types to another into monadic context over INSTANCE
     * @param function the function to lift
     * @param <X> the input type of the input functions first arg
     * @param <Y> the input type of the input functions second arg
     * @param <U> the return type of the input function
     * @param <INSTANCE> the Monad instance
     * @param <XM> the narrowed type of the returned function's first input Monad
     * @param <YM> the narrowed type of the returned function's second input Monad
     * @param <UM> the narrowed type of the returned function's return Monad
     * @return a function (X, Y) -> U in monadic context
     */
    static <X, Y, U, INSTANCE extends Monad, XM extends Monad<INSTANCE, X>, YM extends Monad<INSTANCE, Y>, UM extends Monad<INSTANCE, U>> BiFunction<XM, YM, UM> m2(BiFunction<? super X, ? super Y, ? extends U> function) {
        Objects.requireNonNull(function);
        return (x, y) -> Type.narrow(x.flatMap(a -> y.flatMap(b -> y.pure((U)function.apply(a, b)))));
    }

    interface Type {

        /**
         * Safely cast a {@link Monad} to a instance subclass type
         * @param higher the {@link Monad} to cast
         * @param <INSTANCE> the instance type extending {@link Monad}
         * @param <U> the type parameter of the {@link Monad}
         * @param <OUT> the final narrowed type of the {@link Monad}
         */
        @SuppressWarnings("unchecked")
        static <OUT extends Monad<INSTANCE, U>, INSTANCE extends Monad, U> OUT narrow(Monad<INSTANCE, ? extends U> higher) {
            return (OUT) higher;
        }

        @SuppressWarnings("unchecked")
        static <OUT extends Monad<INSTANCE, U>, INSTANCE extends Monad, U> OUT castParam(Monad<INSTANCE, ?> higher) {
            return (OUT) higher;
        }
    }
}
