package vn.hoapm.util.functional.higher;


import vn.hoapm.util.functional.collections.Linked;

/**
 * Type Class that encodes a commutative operation over T.
 * @param <INSTANCE> The subclass instance of {@link Monoid} that may change between subclasses during operations
 * @param <T> the type parameter that the operation encloses.
 */
public interface Monoid<INSTANCE extends Monoid, T> {

    /**
     * Creates an instance that has no effect when passed to {@link Monoid#mappend(Monoid)}
     */
    Monoid<INSTANCE, T> identity();

    /**
     * combines this and the other in a commutative operation. Should respect order when necessary
     * @param other will be combined with this value
     * @return the result of the combination
     */
    Monoid<INSTANCE, T> mappend(Monoid<INSTANCE, ? extends T> other);

    /**
     * Reduce a list of {@link Monoid}
     * @param list a list of {@link Monoid} of type INSTANCE
     * @return a combined {@link Monoid} being identical to calling {@link Monoid#mappend(Monoid)} on each from list in order;
     */
    default Monoid<INSTANCE, T> mconcat(Linked<Monoid<INSTANCE, ? extends T>> list) {
        // start from the base of the input stack and fast append each element on top of the accumulated monoid
        return list.foldRight(identity(), (mx, m) -> Type.<Monoid<INSTANCE, T>, INSTANCE, T>narrowHigher(m).mappend(mx));
    }

    interface Type {
        @SuppressWarnings("unchecked")
        static <OUT extends Monoid<INSTANCE, U>, INSTANCE extends Monoid, U> OUT narrowHigher(Monoid<INSTANCE, ? extends U> higher) {
            return (OUT) higher;
        }

        @SuppressWarnings("unchecked")
        static <OUT extends Monoid<INSTANCE, U>, INSTANCE extends Monoid, U> OUT castParam(Monoid<INSTANCE, ?> higher) {
            return (OUT) higher;
        }
    }
}
