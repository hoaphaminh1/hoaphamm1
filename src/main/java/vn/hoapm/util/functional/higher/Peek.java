package vn.hoapm.util.functional.higher;

import java.util.function.Consumer;

public interface Peek<T1> {
    void peek(Consumer<? super T1> consumer);
}
