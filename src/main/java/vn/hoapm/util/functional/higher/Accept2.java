package vn.hoapm.util.functional.higher;

import java.util.function.BiConsumer;

public interface Accept2<T1, T2> {
    void accept(BiConsumer<? super T1, ? super T2> c2);
}
