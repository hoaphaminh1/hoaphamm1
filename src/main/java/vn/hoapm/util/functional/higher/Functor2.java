package vn.hoapm.util.functional.higher;

import java.util.function.Function;

/**
 * Type Class that encodes a sequence of operations over a constant INSTANCE, where its Type parameters, T1, T2, may change.
 * {@link Functor2} can only encode a static context that doesn't change between operations. To change context, {@link Monad2} should be used.
 * @param <INSTANCE> The subclass instance of {@link Functor2} that remains constant across operations
 * @param <T1> the first mutable type parameter
 * @param <T2> the first mutable type parameter
 */
public interface Functor2<INSTANCE extends Functor2, T1, T2> {
    /**
     * Map over the type parameters. context should be preserved when chaining calls.
     */
    <U1, U2> Functor2<INSTANCE, U1, U2> map(Function<? super T1, ? extends U1> m1, Function<? super T2, ? extends U2> m2);
}
