package vn.hoapm.util.functional.higher;

import java.util.function.Function;

/**
 * Type Class that encodes a sequence of operations over a constant INSTANCE, where its Type parameter, T, may change.
 * {@link Functor} can only encode a static context that doesn't change between operations. To change context, {@link Monad} should be used.
 * @param <INSTANCE> The subclass instance of {@link Functor} that remains constant across operations
 * @param <T> the mutable type parameter
 */
public interface Functor<INSTANCE extends Functor, T> {

    /**
     * Map over the type parameter. context should be preserved when chaining calls.
     */
    <U> Functor<INSTANCE, U> map(Function<? super T, ? extends U> mapper);

    interface Type {

        @SuppressWarnings("unchecked")
        static <OUT extends Functor<INSTANCE, U>, INSTANCE extends Functor, U> OUT castParam(Functor<INSTANCE, ?> higher) {
            return (OUT) higher;
        }
    }
}
