package vn.hoapm.util.functional.higher;



import java.util.Objects;
import java.util.function.BiFunction;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.UnaryOperator;

/**
 * Type Class that encodes a sequence of operations over a constant INSTANCE, where its Type parameters, T1, T2, may change
 * @param <INSTANCE> The subclass instance of {@link Monad2} that remains constant across operations
 * @param <T1> the first mutable type parameter
 * @param <T2> the second mutable type parameter
 */
public interface Monad2<INSTANCE extends Monad2, T1, T2> extends Functor2<INSTANCE, T1, T2> {

    /**
     * Creates a new {@link Monad2} of arbitrary type parameters
     */
    <U1, U2> Monad2<INSTANCE, U1, U2> pure(U1 a, U2 b);

    /**
     * Map over both type parameters. May change context between subtypes of INSTANCE
     */
    <U1, U2> Monad2<INSTANCE, U1, U2> flatMap(BiFunction<? super T1, ? super T2, Monad2<INSTANCE, ? extends U1, ? extends U2>> mapper);

    /**
     * <p>Lift two unary operators to a single unary operator over a monadic context provided by INSTANCE</p>
     * <p>for example with the {@link Monad2} instance: {@link Tuples.T2}</p>
     * <p>
     * {@code incrementAndLower: T2<int, string> -> T2<int, string> = Monad2.m1Op(increment, toLower)}<br/>
     * {@code incrementAndLower (1, "A") == (2, "a")}
     * </p>
     * @param operator1 the operator to lift for X
     * @param operator2 the operator to lift for Y
     * @param <X> the first type parameter of the {@link Monad2}
     * @param <Y> the second type parameter of the {@link Monad2}
     * @param <INSTANCE> the {@link Monad2} instance
     * @param <FINAL> the narrowed type of the {@link Monad2}
     * @return a unary operator that respects monadic context over both type parameters
     */
    static <X, Y, INSTANCE extends Monad2, FINAL extends Monad2<INSTANCE, X, Y>> UnaryOperator<FINAL> m1Op(UnaryOperator<X> operator1, UnaryOperator<Y> operator2) {
        Objects.requireNonNull(operator1);
        Objects.requireNonNull(operator2);
        return x -> Type.narrow(x.flatMap((a, b) -> x.pure(operator1.apply(a), operator2.apply(b))));
    }

    /**
     * <p>Lift two binary operators to a single binary operator over a monadic context provided by INSTANCE</p>
     * <p>for example with the {@link Monad2} instance: {@link Tuples.T2}</p>
     * <p>
     * {@code merge: (T2<int, string>, T2<int, string>) -> T2<int, string> = Monad2.m2Op(add, concat)}<br/>
     * {@code merge (1, "a") (2, "b") == (3, "ab")}
     * </p>
     * @param operator1 the operator to lift for X
     * @param operator2 the operator to lift for Y
     * @param <X> the first type parameter of the {@link Monad2}
     * @param <Y> the second type parameter of the {@link Monad2}
     * @param <INSTANCE> the {@link Monad2} instance
     * @param <FINAL> the narrowed type of the {@link Monad2}
     * @return a binary operator that respects monadic context over both type parameters
     */
    static <X, Y, INSTANCE extends Monad2, FINAL extends Monad2<INSTANCE, X, Y>> BinaryOperator<FINAL> m2Op(BinaryOperator<X> operator1, BinaryOperator<Y> operator2) {
        Objects.requireNonNull(operator1);
        Objects.requireNonNull(operator2);
        return (x, y) -> Type.narrow(x.flatMap((x1, y1) -> y.flatMap((x2, y2) -> y.pure(operator1.apply(x1, x2), operator2.apply(y1, y2)))));
    }

    /**
     * <p>Lift two functions to a single function over a monadic context provided by INSTANCE</p>
     * <p>for example with the {@link Monad2} instance: {@link Tuples.T2}</p>
     * <p>
     * {@code stringifyAndParse: T2<int, string> -> T2<string, int> = Monad2.m1Op(stringify, parseInt)}<br/>
     * {@code stringifyAndParse: (1, "23") == ("1", 23)}
     * </p>
     * @param function1 the function to lift for X
     * @param function2 the operator to lift for Y
     * @param <X> the first type parameter of the input {@link Monad2}
     * @param <U1> the first type parameter of the output {@link Monad2}
     * @param <Y> the second type parameter of the input {@link Monad2}
     * @param <U2> the second type parameter of the output {@link Monad2}
     * @param <INSTANCE> the {@link Monad2} instance
     * @param <IM> the narrowed type of the input {@link Monad2}
     * @param <OM> the narrowed type of the output {@link Monad2}
     * @return a unary operator that respects monadic context over both type parameters
     */
    static <X, U1, Y, U2, INSTANCE extends Monad2, IM extends Monad2<INSTANCE, X, Y>, OM extends Monad2<INSTANCE, U1, U2>>
    Function<IM, OM> m1(Function<? super X, ? extends U1> function1, Function<? super Y, ? extends U2> function2) {
        Objects.requireNonNull(function1);
        Objects.requireNonNull(function2);
        return x -> Type.narrow(x.flatMap((x1, y1) -> x.pure((U1)function1.apply(x1), (U2)function2.apply(y1))));
    }

    /**
     * <p>Lift two binary functions to a single binary function over a monadic context provided by INSTANCE</p>
     * <p>for example with the {@link Monad2} instance: {@link Tuples.T2}</p>
     * <p>
     * {@code merge: (T2<int, string>, T2<string, bool>) -> T2<T2<int, string>, string> = Monad2.m2Op(zip, concat)}<br/>
     * {@code merge (1, "b: ") ("a", true) == ((1, "a"), "b: true")}
     * </p>
     * @param function1 the function to lift for X1 and X2
     * @param function2 the function to lift for Y1 and Y2
     * @param <X1> the first type parameter of the first input {@link Monad2}
     * @param <Y1> the second type parameter of the first input {@link Monad2}
     * @param <U1> the first type parameter of the output {@link Monad2}
     * @param <X2> the first type parameter of the second input {@link Monad2}
     * @param <Y2> the second type parameter of the second input {@link Monad2}
     * @param <U2> the second type parameter of the output {@link Monad2}
     * @param <INSTANCE> the {@link Monad2} instance
     * @param <IM1> the narrowed type of the first input {@link Monad2}
     * @param <IM2> the narrowed type of the second input {@link Monad2}
     * @param <OM> the narrowed type of the output {@link Monad2}
     * @return a binary operator that respects monadic context over both type parameters
     */
    static <X1, X2, U1, Y1, Y2, U2, INSTANCE extends Monad2, IM1 extends Monad2<INSTANCE, X1, Y1>, IM2 extends Monad2<INSTANCE, X2, Y2>, OM extends Monad2<INSTANCE, U1, U2>>
    BiFunction<IM1, IM2, OM> m2(BiFunction<? super X1, ? super X2, ? extends U1> function1, BiFunction<? super Y1, ? super Y2, ? extends U2> function2) {
        Objects.requireNonNull(function1);
        Objects.requireNonNull(function2);
        return (x, y) -> Type.narrow(x.flatMap((x1, y1) -> y.flatMap((x2, y2) -> y.pure((U1)function1.apply(x1, x2), (U2)function2.apply(y1, y2)))));
    }

    interface Type {

        /**
         * Safely cast a {@link Monad2} to a instance subclass type
         * @param monad the monad to cast
         * @param <INSTANCE> the instance type extending {@link Monad2}
         * @param <U1> the first type parameter of the {@link Monad2}
         * @param <U2> the second type parameter of the {@link Monad2}
         * @param <FINAL> the final narrowed type of the {@link Monad2}
         */
        @SuppressWarnings("unchecked")
        static <FINAL extends Monad2<INSTANCE, U1, U2>, INSTANCE extends Monad2, U1, U2> FINAL narrow(Monad2<INSTANCE, ? extends U1, ? extends U2> monad) {
            return (FINAL) monad;
        }
    }
}
