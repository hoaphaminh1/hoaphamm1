package vn.hoapm.util.functional;

import vn.hoapm.util.functional.collections.Linked;
import vn.hoapm.util.functional.higher.Monad;
import vn.hoapm.util.functional.higher.Monoid;

import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.function.Function;
import java.util.function.Predicate;

public interface Validation<E, O> extends Monad<Validation<E, ?>, O>, Monoid<Validation<?, O>, E> {

    static <E, O> Validation<E, O> of(O value) {
        return new Accumulate<>(value, Linked.empty());
    }

    /**
     * Create a new validation that will add a generated error to the stack if the test fails, open for accumulation
     * @param value The value being tested
     * @param test a positive constraint that must hold for the value
     * @param ifFail will produce an error using the value if the test fails
     * @param <E> the type of the Error
     * @param <O> the type of the value being tested
     * @return {@link Accumulate} with the error stack incremented if the test fails.
     */
    static <E, O> Validation<E, O> accumulate(O value, Predicate<O> test, Function<O, E> ifFail) {
        return new Accumulate<E, O>(value, Linked.empty()).accumulate(test, ifFail);
    }

    /**
     * Create a new validation that will immediately shortcut if the test fails, adding a generated error to the stack
     * @param value The value being tested
     * @param test a positive constraint that must hold for the value
     * @param ifFail will produce an error using the value if the test fails
     * @param <E> the type of the Error
     * @param <O> the type of the value being tested
     * @return {@link Sink} with the error stack incremented if the test fails; else {@link Accumulate}
     */
    static <E, O> Validation<E, O> shortcut(O value, Predicate<O> test, Function<O, E> ifFail) {
        return new Accumulate<E, O>(value, Linked.empty()).shortcut(test, ifFail);
    }

    /**
     * @return the value being tested
     * @throws NoSuchElementException if this is an instance of {@link Sink}
     */
    O value();

    /**
     * @return the current immutable LIFO stack of accumulated errors
     */
    Linked<E> errorStack();

    /**
     * @return if this is an instance of {@link Sink}
     */
    boolean isSink();

    /**
     * Marks the end of validation and the start of Evaluation.
     * @return {@link Either.Left} of {@link Linked} of Errors in chronological order, if any exist. Otherwise {@link Either.Right} of the value.
     */
    default Either<Linked<E>, Void> evaluate() {
        return errorStack().isEmpty() ? Either.right(null) : Either.left(errorStack().reverse());
    }

    /**
     * Test the value and accumulate an error if this isn't a sink
     * @param test a positive constraint that must hold for the value
     * @param ifFail will produce an error using the value if the test fails
     * @return {@link Sink} if already shortcut; a new {@link Accumulate} with the error stack incremented if the test fails; else the current instance
     */
    default Validation<E, O> accumulate(Predicate<O> test, Function<O, E> ifFail) {
        Objects.requireNonNull(test);
        Objects.requireNonNull(ifFail);
        return isSink() || test.test(value()) ? this : new Accumulate<>(value(), errorStack().push(ifFail.apply(value())));
    }

    /**
     * Test the value and sink if this is not yet shortcut
     * @param test a positive constraint that must hold for the value
     * @param ifFail will produce an error using the value if the test fails
     * @return {@link Sink} if already shortcut; a new {@link Sink} with the error stack incremented if the test fails; else the current instance.
     */
    default Validation<E, O> shortcut(Predicate<O> test, Function<O, E> ifFail) {
        Objects.requireNonNull(test);
        Objects.requireNonNull(ifFail);
        return isSink() || test.test(value()) ? this : new Sink<>(errorStack().push(ifFail.apply(value())));
    }

    /**
     * Will continue validation if the condition is true, else shortcut
     * @param condition will skip further validation if yields false
     * @return no context change if already a sink or condition passes, else shortcut.
     */
    default Validation<E, O> filter(Predicate<O> condition) {
        Objects.requireNonNull(condition);
        return isSink() || condition.test(value()) ? this : new Sink<>(errorStack());
    }

    /**
     * Convert the underlying test value
     * @param mapper a function that will map the test value
     * @param <U> the new type of the value being tested.
     * @return {@link Sink} if already shortcut; or new {@link Accumulate} using the mapper result value with the same error stack
     */
    @Override
    default <U> Validation<E, U> map(Function<? super O, ? extends U> mapper) {
        Objects.requireNonNull(mapper);
        return isSink()
            ? Monad.Type.<Validation<E, U>, Validation<E, ?>, U>castParam(this)
            : new Accumulate<>(mapper.apply(value()), errorStack());
    }

    /**
     * Convert the underlying test value and accumulate errors
     * @param mapper a function that will map the test value and possibly the context of this {@link Validation}
     * @param <U> the new type of the value being tested.
     * @return {@link Sink} if already shortcut; or new {@link Accumulate} or {@link Sink} from mapper result with its error stack accumulated from this instance.
     */
    @Override
    default <U> Validation<E, U> flatMap(Function<? super O, Monad<Validation<E, ?>, ? extends U>> mapper) {
        Objects.requireNonNull(mapper);
        if (isSink()) {
            return Monad.Type.<Validation<E, U>, Validation<E, ?>, U>castParam(this);
        }
        Validation<E, U> next = Monad.Type.<Validation<E, U>, Validation<E, ?>, U>narrow(mapper.apply(value()));
        Linked<E> accumulated = next.errorStack().mappend(errorStack());
        return next.isSink()
            ? new Sink<>(accumulated)
            : new Accumulate<>(next.value(), accumulated);
    }

    /**
     * {@inheritDoc}
     * @param value the value to validate
     * @param <U> the type of the value
     * @return A new {@link Accumulate} for the value, accumulating on to a branch of the current errorStack
     */
    @Override
    default <U> Validation<E, U> pure(U value) {
        return new Accumulate<>(value, errorStack());
    }

    /**
     * {@inheritDoc}
     * @return a new {@link Accumulate} using the current value and an empty error stack
     */
    @Override
    default Validation<E, O> identity() {
        return of(value());
    }

    /**
     * {@inheritDoc}
     * @param list a list of {@link Validation}
     * @return A single validation that has each error accumulated until the first sink is encountered
     */
    @Override
    default Validation<E, O> mconcat(Linked<Monoid<Validation<?, O>, ? extends E>> list) {
        return Monoid.Type.narrowHigher(Monoid.super.mconcat(list));
    }

    /**
     * {@inheritDoc}
     * @param other A {@link Validation} that will be combined with this value
     * @return this if {@link Sink}, else the other with this instances errorStack appended to its
     */
    @Override
    default Validation<E, O> mappend(Monoid<Validation<?, O>, ? extends E> other) {
        return flatMap(o -> Monoid.Type.narrowHigher(other));
    }

    /**
     * {@link Accumulate} tests values and accumulates error messages when they fail.
     * @param <E> the type of the Error
     * @param <O> the type of the value being tested
     */
    class Accumulate<E, O> implements Validation<E, O> {

        private final Linked<E> errorStack;
        private final O value;

        @Override
        public boolean isSink() {
            return false;
        }

        @Override
        public Linked<E> errorStack() {
            return errorStack;
        }

        @Override
        public O value() {
            return value;
        }

        private Accumulate(O value, Linked<E> errors) {
            this.value = value;
            this.errorStack = errors;
        }
    }

    /**
     * A sink has its state frozen and can no longer test values.
     * @param <E> the type of the Error
     * @param <O> the type of the value being tested if this were an instance of {@link Accumulate}
     */
    class Sink<E, O> implements Validation<E, O> {

        private final Linked<E> errorStack;

        @Override
        public boolean isSink() {
            return true;
        }

        @Override
        public Linked<E> errorStack() {
            return errorStack;
        }

        @Override
        public O value() {
            throw new NoSuchElementException("Sink has no value to test");
        }

        private Sink(Linked<E> errorStack) {
            this.errorStack = errorStack;
        }
    }
}
