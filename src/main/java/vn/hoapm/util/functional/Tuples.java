package vn.hoapm.util.functional;


import vn.hoapm.util.functional.higher.Accept2;
import vn.hoapm.util.functional.higher.Compose2;
import vn.hoapm.util.functional.higher.Monad2;
import vn.hoapm.util.functional.higher.Peek2;

import java.util.Objects;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;

public interface Tuples {

    static <X, Y> T2<X, Y> t2(X x, Y y) {
        return new T2.Tuple2<>(x, y);
    }

    static <X, Y, Z> T3<X, Y, Z> t3(X x, Y y, Z z) {
        return new T3.Tuple3<>(
            Objects.requireNonNull(x), Objects.requireNonNull(y), Objects.requireNonNull(z));
    }

    static boolean notEqualElements(T2<?, ?> tuple) {
        Objects.requireNonNull(tuple, "tuple");
        return !tuple.compose(Objects::equals);
    }

    interface T3<A, B, C> {

        A $1();
        B $2();
        C $3();

        default boolean isIdentity() {
            return false;
        }

        default int arity() {
            return 3;
        }

        void forEach(Consumer<A> consumerA, Consumer<B> consumerB, Consumer<C> consumerC);

        <E> E compose(TriFunction<A, B, C, E> composer);

        interface TriFunction<A, B, C, R> {
            R apply(A a, B b, C c);
        }

        class Tuple3<A, B, C> implements T3<A, B, C> {
            private final A a;
            private final B b;
            private final C c;

            private Tuple3(A a, B b, C c) {
                this.a = a;
                this.b = b;
                this.c = c;
            }

            @Override
            public A $1() {
                return a;
            }

            @Override
            public B $2() {
                return b;
            }

            @Override
            public C $3() {
                return c;
            }

            @Override
            public void forEach(Consumer<A> consumerA, Consumer<B> consumerB, Consumer<C> consumerC) {
                Objects.requireNonNull(consumerA, "consumerA");
                Objects.requireNonNull(consumerB, "consumerB");
                Objects.requireNonNull(consumerC, "consumerC");
                consumerA.accept(a);
                consumerB.accept(b);
                consumerC.accept(c);
            }

            @Override
            public <E> E compose(TriFunction<A, B, C, E> composer) {
                Objects.requireNonNull(composer, "composer");
                return composer.apply(a, b, c);
            }
        }
    }

    interface T2<A, B> extends Monad2<T2, A, B>, Compose2<A, B>, Peek2<A, B>, Accept2<A, B>, Comparable<T2<A, B>> {

        A $1();
        B $2();

        default T2<B, A> swap() {
            return t2($2(), $1());
        }

        @Override
        default <U1, U2> T2<U1, U2> pure(U1 a, U2 b) {
            return t2(a, b);
        }

        @Override
        default <U1, U2> T2<U1, U2> map(Function<? super A, ? extends U1> map1, Function<? super B, ? extends U2> map2) {
            Objects.requireNonNull(map1);
            Objects.requireNonNull(map2);
            return t2(map1.apply($1()), map2.apply($2()));
        }

        @Override
        default <O> O compose(BiFunction<? super A, ? super B, ? extends O> mapper) {
            Objects.requireNonNull(mapper);
            return mapper.apply($1(), $2());
        }

        @Override
        default void peek(Consumer<? super A> peek1, Consumer<? super B> peek2) {
            Objects.requireNonNull(peek1);
            Objects.requireNonNull(peek2);
            peek1.accept($1());
            peek2.accept($2());
        }

        @Override
        default void accept(BiConsumer<? super A, ? super B> consumer) {
            Objects.requireNonNull(consumer);
            consumer.accept($1(), $2());
        }

        @Override
        default <U1, U2> T2<U1, U2> flatMap(BiFunction<? super A, ? super B, Monad2<T2, ? extends U1, ? extends U2>> mapper) {
            Objects.requireNonNull(mapper);
            return Monad2.Type.<T2<U1, U2>, T2, U1, U2>narrow(mapper.apply($1(), $2()));
        }

        @Override
        default int compareTo(T2<A, B> other) {
            return compare(this, other);
        }

        @SuppressWarnings("unchecked")
        default <C1 extends Comparable<C1>, C2 extends Comparable<C2>> int compare(T2<?,?> thisOne, T2<?,?> thatOne) {
            try {
                T2<C1, C2> a = (T2<C1, C2>) thisOne;
                T2<C1, C2> b = (T2<C1, C2>) thatOne;

                final int result;

                if ((result = a.$1().compareTo(b.$1())) != 0) {
                    return result;
                }

                return a.$2().compareTo(b.$2());

            } catch (ClassCastException e) {
                throw new IllegalStateException("Tuples do not have comparable values.", e);
            }
        }

        class Tuple2<A, B> implements T2<A, B>, Comparable<T2<A, B>>{

            private final A a;
            private final B b;

            private Tuple2(A a, B b) {
                this.a = a;
                this.b = b;
            }

            @Override
            public A $1() {
                return a;
            }

            @Override
            public B $2() {
                return b;
            }

            @Override
            public int hashCode() {
                return Objects.hash($1(), $2());
            }

            @Override
            public boolean equals(Object obj) {
                return obj == this || obj instanceof T2
                    && Objects.equals($1(), ((T2) obj).$1())
                    && Objects.equals($2(), ((T2) obj).$2());
            }
            @Override
            public String toString() {
                return  "(" + $1() + ", " + $2() + ")";
            }
        }
    }


}
