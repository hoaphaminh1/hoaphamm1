package vn.hoapm.util.functional;

public interface Ignore {

    static void of(Executable supplier) {
        try {
            supplier.execute();
        } catch (Exception ignored) {
        }
    }
}
