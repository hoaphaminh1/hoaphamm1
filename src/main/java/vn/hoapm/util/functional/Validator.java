package vn.hoapm.util.functional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.util.CollectionUtils;

import java.util.Collection;
import java.util.Objects;
import java.util.concurrent.Callable;
import java.util.function.Function;


public interface Validator {

    static boolean test(Callable<Boolean> conditionSupplier) {
        return Maybe.of(Try.of(conditionSupplier).orElse(Boolean.FALSE)).orElse(false);
    }

    static boolean equals(Callable<Object> supplier1, Callable<Object> supplier2) {
        return test(() -> Objects.equals(supplier1.call(), supplier2.call()));
    }

    static <T> boolean equals(T o1, T o2, Function<T, Object> selector) {
        return equals(() -> selector.apply(o1), () -> selector.apply(o2));
    }

    static boolean equals(Object constant, Callable<Object> supplier) {
        return equals(() -> constant, supplier);
    }

    static boolean equalsIgnoreCase(Callable<String> supplier1, Callable<String> supplier2) {
        return test(() -> StringUtils.equalsIgnoreCase(supplier1.call(), supplier2.call()));
    }

    static boolean equalsIgnoreCase(String constant, Callable<String> supplier) {
        return equalsIgnoreCase(() -> constant, supplier);
    }

    static <T> boolean contains(Collection<T> collection, Callable<T> supplier) {
        return test(() -> collection.contains(supplier.call()));
    }

    static boolean nonNull(Callable<Object> supplier) {
        return test(() -> Objects.nonNull(supplier.call()));
    }

    static boolean notEmpty(Callable<Collection> supplier) {
        return test(() -> !CollectionUtils.isEmpty(supplier.call()));
    }

}
