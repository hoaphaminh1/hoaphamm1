package vn.hoapm.springbootV2.entities;


import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Collection;

@Entity
@Table(name = "user_infos")
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserInfo implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    // using GenerationType.Auto will using table generator -> performance penanties
    private Long id;

    @Column(name = "avatar_url")
    private String avatarUrl;

    @Column(name = "bio")
    private String bio;

    @Column(name = "address")
    private String address;

    @Column(name = "gender")
    private Integer gender;

    @Column(name = "follower_count")
    private Integer followerCount;

    @Column(name = "status")
    private Integer status;

    @Column(name = "created_at")
    private Timestamp createdAt;

    @Column(name = "utimestamp")
    private Timestamp utimestamp;

    @Column(name = "following_count")
    private Integer followingCount;

    @Column(name = "track_count")
    private Integer trackCount;

    @Column(name = "playlist_count")
    private Integer playlistCount;

    @Column(name = "online")
    private Integer online;

    @OneToOne
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne
    @JoinColumn(name = "plan_id")
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Plan plan;

    @OneToMany(mappedBy = "userInfo", fetch =FetchType.LAZY ,cascade = CascadeType.ALL)
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Collection<Playlist> playlists;


}
