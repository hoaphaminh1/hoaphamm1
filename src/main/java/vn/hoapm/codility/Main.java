package vn.hoapm.codility;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        System.out.println(solution(123));

    }
    public static int solution (int num) {
        if (num == 0)  {
            return 0;
        }
        if (num < 0) {
            throw new NumberFormatException("N should be non-negative number");
        }
        List<Integer> listNums = new ArrayList<>();
        do {
            listNums.add(num%10);
            num /= 10;
        } while(num > 0);
        Collections.sort(listNums, Collections.reverseOrder());

        int result = listNums.get(0);
        for(int i = 1; i < listNums.size(); i++) {
            result = result * 10;
            result = result + listNums.get(i);
            if(result > 100000000)
                return -1;
        }
        return result;
    }

}
