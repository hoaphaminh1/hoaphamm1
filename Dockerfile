FROM maven:3.8.3-jdk-8
COPY src /home/app/src
COPY target /home/app/target
COPY entrypoint.sh test.sh /home/app/
COPY pom.xml /home/app
WORKDIR /home/app
USER root
RUN chmod a+x entrypoint.sh test.sh
#CMD ["mvn","spring-boot","run"]